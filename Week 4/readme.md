# Topics

> Below you'll find an overview of the topics you should familiarize yourself with for the week. You can also find some resources which should help you familiarize yourself with these topics.

## Servlets

* Deployment Descriptor
* Common Deployment Descriptor Tags
* Endpoints
* Servlet Hierarchy
* Servlet Life Cycle
* HTTP Verbs
* HTTP Handler Methods
* HTTP Request
* HTTP Response
* JSON
* Jackson ObjectMapper
* Retrieving Form Parameters
* 

## Postman

* Sending an HTTP Request Via Postman

## TypeScript

* Transpilation
* Features of TypeScript
* Access Modifiers
* Data Types
* Functions
* Classes

## Angular




