package com.revature.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Session
 */
public class Session extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Session() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/*
		 * Currently, a user can access any resource on this server
		 * as long as they know the URL for accessing said resource.
		 * This is not ideal as no resources are truly protected
		 * if this is the case.
		 * 
		 * In order to protect our resources a bit better (and personalize
		 * the user experience), we can take advantage of session
		 * management.
		 * 
		 * We'll use JSessions for this exercise!
		 */
		
		final String email = request.getParameter("username");
		final String password = request.getParameter("password");
		
		//Let's just create a stub user. We'll grant a session conditionally.
		
		if(email.equals("deku@mha.com") && password.equals("password")) {
			//This creates a session for this client and stores info
			//about it on the server.
			HttpSession session = request.getSession();
			
			//You can create session attributes for convenience.
			session.setAttribute("user email", email);
			
			response.sendRedirect("./views/home.html");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
