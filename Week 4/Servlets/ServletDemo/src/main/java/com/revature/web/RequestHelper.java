package com.revature.web;

import java.io.IOException;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.revature.model.MonsterCard;
import com.revature.service.MonsterCardService;

public class RequestHelper {

	/*
	 * This is a helper class to which we pass
	 * our request and responses for further processing.
	 */
	
	//I'll use this method to process GET requests.
	public static Object processGet(HttpServletRequest request, HttpServletResponse response) {
		
		//How do we decide what to return?
		
		final String URL = request.getRequestURI();
		System.out.println(URL);
		
		final String RESOURCE = URL.replace("/ServletDemo/myapi", "");
		System.out.println(RESOURCE);
		
		switch(RESOURCE) {
		case "/allcards":
			return new MonsterCardService().findAllMonsterCards();
		default:
			return "No such endpoint or resource";
		}	
	}
	
	public static void processPost(HttpServletRequest request, HttpServletResponse response) {
		
		final String URL = request.getRequestURI();
		System.out.println(URL);
		
		final String RESOURCE = URL.replace("/ServletDemo/myapi", "");
		System.out.println(RESOURCE);
		
		switch(RESOURCE) {
		case "/newcard":
			try {
				final String JSON = request.getReader()
						.lines()
						.collect(Collectors.joining());
				System.out.println(JSON);
			ObjectMapper imTheMap = new ObjectMapper();
			
			MonsterCard newCard = imTheMap.readValue(JSON, MonsterCard.class);
			new MonsterCardService().insert(newCard);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
