package com.revature.repository;

import java.util.List;

import com.revature.model.MonsterCard;

/*
 * It is customary to refer to classes and interfaces which perform tasks
 * related to data access as "repositories".
 * 
 * It is also customary to define interfaces (so use interface-driven
 * development) which specify behaviors which your repository implementations
 * must perform.
 * 
 * It is possible (and not unlikely) to have multiple implementations of a 
 * repository. For instance, you might implement this repository (so a class would
 * implement it) for use with Postgres. You could also have an implementation which
 * relies on MongoDB or some other dialect. In such cases, the code would be easier
 * to refactor as you can keep references to the super type (MonsterCardRepository)
 * and simply swap out implementations as needed.
 */
public interface MonsterCardRepository {

	/*
	 * Let's define some methods that all implementations of this repository should
	 * implement.
	 * 
	 * These methods are typically your basic CRUD methods: 
	 */
	
	void insert(MonsterCard monsterCard); //CREATE
	List<MonsterCard> findAllMonsterCards(); //READ
	void update(MonsterCard monsterCard); //UPDATE
	void delete(MonsterCard monsterCard); //DELETE
}
