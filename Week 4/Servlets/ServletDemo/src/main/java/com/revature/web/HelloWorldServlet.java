package com.revature.web;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.revature.service.MonsterCardService;

public class HelloWorldServlet extends HttpServlet{
	
	/*
	 * When we were making simple console applications, we had a main
	 * method which we utlimately passed our data to in order to display
	 * it to the end user. This time, we're building a web application,
	 * so there is client-server communication. As a result, we need to
	 * pass data which has been retrieved from our DB to the client.
	 * In order to do so, we need to write it to the response body.
	 * 
	 * To that effect, we'll need a MonsterCardService dependency in
	 * our web layer.
	 */
	
	private MonsterCardService monsterCardService;
	
	public HelloWorldServlet() {
		this.monsterCardService = new MonsterCardService();
	}
	
	/*
	 * Note that all of these methods are a part of our Servlet life cycle;
	 * they're all called at some point.
	 */
	@Override
	public void init() throws ServletException {
		// TODO Auto-generated method stub
		super.init();
	}
	
	/*
	 * Note that this verison of init() is called to pass in servlet config
	 * information.
	 */
	
	@Override
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
		super.init(config);
	}
	
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		super.service(req, resp);
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		super.destroy();
	}


	/*
	 * Recall that there are several handler methods for the various
	 * HTTP verbs. That is to say, if I want to handle a GET request,
	 * I need to use a handler method specifying that this endpoint I'm
	 * setting up does accept GET requests.
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
		/*
		 * We can write practically anything to the response body.
		 * We can set our content type that we are writing to the response
		 * body as:
		 * 
		 * JSON
		 * plain text
		 * HTML
		 */
		
		response.setContentType("text/plain");
		
		//Accessing our Servlet Config
//		response.getWriter()
//		.write("The servlet config: " + this.getInitParameter("location of file"));
	
		//Accessing our Servlet Context
//		response.getWriter()
//		.write("The servlet context is: " + this.getServletContext().getInitParameter("global context param"));
	
		/*
		 * While you can choose to pass data from server to client
		 *  or client to server in any format, it is standard to pass
		 *  it in one of the more widely used formats for data exchange.
		 *  One of those is JSON.
		 *  
		 *  Fortunately, there are already tools which can convert Java
		 *  objects into JSON format. This is known as serialization.
		 *  
		 *  The primary object used when working with Jackson is the
		 *  ObjectMapper.
		 */
		
		ObjectMapper imTheMap = new ObjectMapper();
		final String JSON = imTheMap
				.writeValueAsString(this.monsterCardService.findAllMonsterCards());
		
		//Write JSON string to the response body.
		response.getWriter().write(JSON);
	
	}
	
	/*
	 * Let's implement a method for handling a POST request.
	 */
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
		
		/*
		 * This is a handler method for a POST request. This means that
		 * we're expecting to receive some data from the client.
		 * 
		 * We can access the data the client has sent to the server
		 * using the request object.
		 */
		
		final String username = request.getParameter("username");
		final String password = request.getParameter("password");
		
		response.setStatus(200);
		
		response.getWriter()
		.write("Success! Your username is: " + username + " and your password is: " + password);
	}
}
