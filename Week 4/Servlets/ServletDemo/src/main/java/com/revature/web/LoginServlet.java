package com.revature.web;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class LoginServlet
 */
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */

	/*
	 * Understand that in this exercise, we redirected the user unconditionally. We
	 * do need to:
	 * 
	 * 1) Actually authenticate users' credentials. 2) We need to make sure that
	 * they have sessions before we reveal certain resources to them (and we have to
	 * do this before revealing every resource that should be protected).
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/*
		 * We are using this servlet to handle login. Some typical actions we perform
		 * when handling login are:
		 * 
		 * 1) Validating that a user exists within our system. Note that the actual
		 * logic for validating a user's credentials would be present in a service
		 * layer. You would simply delegate to the service method to handle that.
		 * 
		 * 2) Forwarding/redirecting the client to another resource
		 */

		// Let's get the information that was passed back to us;
		// Again, you would delegate to your service layer to handle
		// validating the password. Don't directly place your logic
		// for validating credentials here in the web layer.

		final String email = request.getParameter("username");
		final String password = request.getParameter("password");

		// Assuming that the credentials are correct, redirect the client
		// to a new resource.

//		response.sendRedirect("./views/home.html");

		// What if we forward instead?

		// We could always check that a user has a session before redirecting
		// or forwarding the client. But there's a better option.

			RequestDispatcher dispatcher = request.getRequestDispatcher("./views/home.html");
			dispatcher.forward(request, response);

		/*
		 * Neither sendRedirect or forward is "better"; they have different uses.
		 * 
		 * sendRedirect can be used to redirect to resources that may not be hosted on
		 * this server. It is also, however, slower than forward because it triggers a
		 * new reqeust-response cycle, meaning that there are two requests sent and two
		 * response sent back to the client. Note that sendRedirect also exposes the
		 * location of the resource to the client.
		 * 
		 * forward can be used to forward the request to a new resource, but that
		 * resource has to be on this server. Even so, forward is faster than
		 * sendRedirect because you don't trigger a new request-response cycle. It also
		 * obscures the final resource location.
		 */

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
