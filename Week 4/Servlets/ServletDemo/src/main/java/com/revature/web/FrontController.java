package com.revature.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Servlet implementation class FrontController
 */
public class FrontController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public FrontController() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */

	/*
	 * This is our FrontController. It is going to be the primary point of entry
	 * into our web application; in other words, all requests must come through this
	 * servlet.
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/*
		 * We are not going to manipulate or view the contents of our request here.
		 * We're not even going to alter the response headers, etc. This layer only
		 * handles intercepting requests and it delegates its tasks to other layers.
		 */

		// Delegate to RequestHelper in the case of GET requests
		if (request.getMethod().equals("GET")) {
			response.getWriter()
			.write(new ObjectMapper()
					.writeValueAsString(RequestHelper.processGet(request, response)));
		}
		
		//Delegage to RequestHelper in case of POST requests
		else if(request.getMethod().equals("POST")) {
			RequestHelper.processPost(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
