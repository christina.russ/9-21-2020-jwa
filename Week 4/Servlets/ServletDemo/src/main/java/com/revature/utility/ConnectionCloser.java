package com.revature.utility;

import java.sql.SQLException;

public class ConnectionCloser {

	/*
	 * While not necessary, I've written a utility method for closing my resources.
	 * I've done so in order to handle exceptions in one central place. Note that
	 * you can pass any AutoCloseable to this method.
	 */
	public static void closeResource(AutoCloseable auto) {
		try{
			auto.close();
		}catch(SQLException e) {
			e.printStackTrace();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
}
