package com.revature.service;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.revature.model.GenericCard;
import com.revature.model.MonsterCard;
import com.revature.repository.MonsterCardRepository;
import com.revature.repository.MonsterCardRepositoryImpl;

/*
 * This is our service layer. It is designed to perform the "business logic" of an
 * application. For example, if you want to sanitize data or mutate it in some way
 * for presentation purposes, you perform this tasks here. Tasks related to computations
 * and security, for instance, might also be present here.
 * 
 * Typically, you will move data through your service layer once you've retrieve it from
 * the data source and transform that data her if needed.
 */

public class MonsterCardService {

	//Data must be passed through this layer via the Repository
	/*
	 * We often refer to fields such as this one as "dependencies" because the implementation
	 * of the MonsterCardService depends on the implementation of MonsterCardRepository.
	 * That is to say, if I were to change the implementation of the MonsterCardRepositoryImpl,
	 * I would have to possibly modify the MonsterCardService.
	 * 
	 * These two classes are tightly coupled.
	 */
	private MonsterCardRepository monsterCardRepository;
	
	public MonsterCardService() {
		//Making sure I have a working instance of my dependency
		this.monsterCardRepository = new MonsterCardRepositoryImpl();
	}
	
	public List<MonsterCard> findAllMonsterCards(){
		//In this case, we aren't performing business logic, though we could, and this
		//would be the appropriate place to handle it.
		return this.monsterCardRepository.findAllMonsterCards();
	}
	
	/*
	 * This method sorts the cards based on the comparator passed in. This logic
	 * is not directly related to data access; it is about transforming the data in
	 * some way for business purposes (e.g. maybe the user interface allows users to
	 * filter and sort the cards per the application requirements).
	 */
	public List<MonsterCard> findAllMonsterCardsSorted(Comparator<MonsterCard> c){
		List<MonsterCard> cards = this.monsterCardRepository.findAllMonsterCards();
		Collections.sort(cards, c);
		return cards;
	}
	
	public void update(MonsterCard monsterCard) {
		this.monsterCardRepository.update(monsterCard);
	}
	
	public void insert(MonsterCard monsterCard) {
		this.monsterCardRepository.insert(monsterCard);
	}
	
}
