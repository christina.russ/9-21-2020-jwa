let originalColors = true

function changeColor(){
    //Find my web elements using some sort of locator
    let h1 = document.getElementsByTagName('h1')
    let h2 = document.getElementsByTagName('h2')

    //Let's change the color of h1 and h2; we've just swapped the colors of the text
    if(this.originalColors){
        h1[0].style.color = 'orange'
        h2[0].style.color = 'gray'
    }else{
        h1[0].style.color = 'gray'
        h2[0].style.color = 'orange'
    }

    //Change originalColors
    this.originalColors = !this.originalColors
}

 //Currently, this function does not listen for any events. Thus, we can't use it to dynamically modify the colors of our text. In order to do so, we'll have to use what we refer to as "event listeners". Event listeners "listen" for certain events on certain web elements. Note that event listeners are functions that associate a JavaScript Event (a JS object) with a callback function. A callback function is a function that is invoked from within another function.

    //Let's select the containing for the text we wish to modify

    let div = document.getElementById('message')

    //Now let's add an event listener to this div

    //click is the event, changeColor is the callback function

    div.addEventListener('click', changeColor)

    /* We can also (and should) use JavaScript for tasks that are not purely cosmetic. We can also use it to perform client-side validation. Note that client-side validation is NOT enough to validate user information, etc, so it should never be used alone; do NOT skip server-side validation.

    In order to prevent default behavior associated with a click event, we'll pass our event to the validatePassword function.
    */

    let hasNoMessage = true

    function validatePassword(event){

        //We need to grab the element whose value we're trying to validate
        let inputBoxes = document.getElementsByTagName('input')
        let userPass = inputBoxes[1].value

        if(userPass.length < 8){
            //We should tell the user WHY the form is not being submitted.
            //Let's first select the form on our DOM

            if(hasNoMessage){
                let form = document.getElementById('form')

                //Let's create the element we want to append to our form

                let message = document.createElement('p')

                //Specify some text for the new paragraph as it is currently empty

                message.innerText = 'Password must be at least 8 characters long!'

                //Now that we've specified the text content of our paragraph, let's append it to the form!

                form.append(message)

                hasNoMessage = false

            }

            //Uh oh! We're infinitely appending the error message to the form! We only want to append it once, so we'll create some way of keeping track of whether or not we've already appended this message!

            //call preventDefault; be sure to check that the event is actually cancelable
            if(event.cancelable){
            event.preventDefault()
            }
    }
    }

    //Let's create an event listener for the form's button:

    /*
     * We have been thwarted as even though the length of the password is checked in our function, we did not prevent the default behavior that is associated with clicking a button. As a result, the form is still submitted even if the user's password is less than 8 characters.

     In order to make sure that the form is not submitted in such cases, we need to the preventDefault function on our event.
     */

    let button = document.querySelector('button')

    button.addEventListener('click', validatePassword)

    /*
     * Whenever we add event listeners to elements, there is actually an order in which the events are fired off. This becomes more apparent when we have multiple event listeners associated with the same element (indirectly).

     Let's add an alert to our div with the id of "message":
     */

     let messageDiv = document.getElementById('message')


     /*
      * Note that we've modified the event propagation here by passing in a value
        of "true" as a third argument. This specifies that we want to use capturing (which is false by default).
      */
     
     messageDiv.addEventListener('click', () => {
         //Pops up an annoying, spammy alert box in your browser
         //Less biased: Pops an alert box in your browser.
         window.alert("You've won a free Iphone because you clicked the div!")
     }, true)

     // Let's also add an alert to the h1 inside of our div. Note that it's not possible to click the h1 without clicking the div.

     let innerH1 = document.querySelector('h1')

     innerH1.addEventListener('click', () => {
         window.alert("You've won a free android phone because you've clicked the h1")
     })

     /*
      * The default order in which events are propagated is from the innermost element to the outermost element. This is referred to as bubbling (and it visually resembles blowing a bubble).

      You can change the default order in which events are propagated and use capturing instead. Capturing allows us to make our outermost events occur first and innermost events occur last!
      */

