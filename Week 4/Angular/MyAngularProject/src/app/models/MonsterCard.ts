/*
 * TypeScript actually has classes and interfaces.
 */

import { OnInit } from '@angular/core';

export class MonsterCard{

    //TypeScript has static typing.
    public id:number;
    public name:string;
    public health:number;
    public faceup:boolean;
    
    constructor(id:number, name:string, health:number, faceup:boolean){
        this.id = id;
        this.name = name;
        this.health = health;
        this.faceup = faceup;
    }

    aMonsterCardFunction():number{
        return 8;
    }

}