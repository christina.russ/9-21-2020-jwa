import { Component, OnInit, Renderer2, ElementRef } from '@angular/core';
import { TodosService } from 'src/app/services/todos.service';

@Component({
  selector: 'app-todos-home',
  templateUrl: './todos-home.component.html',
  styleUrls: ['./todos-home.component.css']
})
export class TodosHomeComponent implements OnInit {

  /**
   * Let's add a boolean property called "visibility". We'll make it "true"
   * by default.
   */

  visibility: boolean = true;

  /**
   * This property will track the preferred name the user enters.
   */

  name: string = "";

  /**
   * This property tracks the greeter div's visibility. It has an *initial value of "true" so that the greeter div will be visible
   * when we use *ngIf on it.
   */

  greeterDiv: boolean = true;

  /**
   * Mock Todos. Note that these are simply JavaScript objects.
   */

  todo1 = {
    id: 1,
    content: "Make a cup of coffee before I practice my Java!",
    status: "Incomplete"
  }

  todo2 = {
    id: 2,
    content: "Merge sort all of my socks before laundry day. P.S. I hate laundry days!",
    status: "Incomplete"
  }

  todo3 = {
    id: 3,
    content: "Feed my pet python a snack.",
    status: "Incomplete"
  }

  todo4 = {
    id: 4,
    content: "Have another cup of coffee before I go rehearse the script for my upcoming play: JavaScript.",
    status: "Incomplete"
  }

  todo5 = {
    id: 5,
    content: "Make Kotlin number 1.",
    status: "Complete"
  }

  todo6 = {
    id: 6,
    content: "WFH",
    status: "Complete"
  }

  /**
   * Let's create an array to hold our mock todos!
   */

  todos = [this.todo1, this.todo2, this.todo3, this.todo4, this.todo5, this.todo6];

  constructor(private todosService:TodosService) { }

  getAllTodos(){
    this.todosService.getAllTodos()
  }

  ngOnInit() {
    this.getAllTodos();
  }

  /**
   * We're adding an event handler that can be invoked when a user clicks
   * on a button!
   */

  toggleVisibility() {
    this.visibility = !this.visibility;
  }

  submitName() {
    this.greeterDiv = false;
  }

}
