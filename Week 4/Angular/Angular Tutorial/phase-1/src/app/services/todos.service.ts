import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TodosService {

  constructor(private httpClient:HttpClient) { }

  getAllTodos(){
     console.log(this.httpClient.get("34.82.182.44/todos"));
  }
}
