# Building a Simple Angular Application: Part One
---
## Purpose

We designed this beginner's tutorial with the purpose of helping developers who are new to the Angular framework build simple Angular applications. The tutorial covers Angular fundamentals such as the Angular command line interface and the structure of an Angular project. It also provides tips on how to set up your development environment in order to create and run an Angular application in development mode. 

### Getting Started: Prerequisites

Before you can start creating your Angular application, you'll need to install and familiarize yourself with the following languages and tools:
- **Node.js**: We will use Node's package manager (npm) to install the Angular command line interface and manage the dependencies within our project. Node.js will also allow us to run our Angular application in development mode as Angular applications are written in TypeScript. Recall that TypeScript does not have its own runtime and must be transpiled into JavaScript. In order to verify that you have Node.js and npm installed properly, run the following command:  

        npm -version
- **TypeScript**: We write Angular applications in TypeScript that is later transpiled into JavaScript. As a result, you'll need to be comfortable with TypeScript.
- **Visual Studio Code**: For this tutorial, we will use Microsoft Visual Studio Code in order to edit our source code. You may choose to use another text editor if you feel comfortable doing so.

## Step 1: Install The Angular Command Line Interface

As mentioned before, we will be using the Angular command line interface (CLI) while developing our Angular project. This CLI streamlines the process of creating key files within our project. In order to use the command line interface, we must first install it to our local machine using npm. In order to do so, run the following command in your terminal:

    npm install -g @angular/cli

**Note**: The "-g" flag installs the Angular CLI in a single place in your system so that you can have access to it within all of your Angular projects. If you only wish to install the Anaular CLI in the directory in which you will be working out of for your project, omit this flag.

In order to verify that you have properly installed the Angular CLI, please open your terminal and run this command:

    ng -version

## Step 2: Generate A New Angular Project

Now that we've installed the CLI, we can use it to generate a new Angular project! In order to generate your first Angular project, run the following command:

    ng new <project-name> --skip-git --routing

For example, if you want to generate an Angular project with the name "angular-demo", type the following:

    ng new angular-demo --skip-git --routing

**Note:** The "--skip-git" forgoes the default initialization of your Angular project's directory as a Git repository. This flag is useful as developers usually already have a Git repository which they intend to use to store project files. The "--routing" flag specifies that we want to use Angular routing within our Angular project in order to create to a single page application.

After you've run this command, you may have to answer questions about whether or not you want to use CSS or Sass. For this demo, we'll be using CSS, but feel free to choose one of the alternative choices if you feel comfortable doing so.

Once all of the necessary files have been generated, you should have a new directory present where you ran the command. The directory's title should match your project name. The project name for this demo, for instance, is "angular-demo", so we now have a directory called "angular-demo".

## Step 3: Open Your Angular Project Using A Text Editor

Now that you've generated an Angular project, you'll want to open it using a text editor of your choice. As we mentioned earlier, we will be using Visual Studio Code to edit our source code. That said, go ahead and open the newly generated directory in order to view your project files!

Once you've opened your project, you should see several files which have been generated for you. These files contain important information such as project configuration details, lists of dependencies used throughout your project, and source code. 

![Image of Files Generated After Running the Command](./images/angular-project-structure.PNG)

Some of the directories and files which you'll want to immediately familiarize yourself with for this tutorial include:
- **e2e (directory)**: This directory contains end-to-end tests which developers can use to test the user interface. These tests rely on a JavaScript testing framework known as *Protractor*.
- **src (directory)**: This directory contains the source code for our Angular project. It is home to the templates and TypeScript files which contain our application's logic. As such, we'll primarily work out of this folder for the duration of the tutorial.
- **angular.json (file)**: This file contains project configuration information. It details the version of the project, the location of several key configuration files, and information about how the developer wishes to handle their Angular project's files in both development and production.
- **package.json (file)**: This file is not specific to Angular; in fact, projects developed in a Node environment contain a *package.json* file. This particular file details all of the packages your project depends on and provides information about those packages' versions.
- **package-lock.json (file)**: Like the *package.json* file, the *package-lock.json* is also used in Node environments for dependency management. It differs from *package.json* in that it describes an exact dependency tree which guarantees that developers are using the exact same versions of dependencies, including transitive dependencies, from build to build. In other words, every time a developer runs the `npm install` command, the version of the dependency specified in *package-lock.json* will be used.
- **node-modules (directory)**: This directory contains all of the dependencies the developer is using to develop an application in a Node environment. For this tutorial, you'll notice that Angular (listed as *@angular*) is listed as a dependency. No surprise there!
    - **Note**: An Angular project that is generated using the CLI will have many dependencies provided by default. Don't worry, however, as Angular projects come with *gitignore* files which ensure that the *node_modules* directory is not pushed to a remote Git repository.

## Step 4: Locate The "app.module.ts" File

Before we start building our Angular application, there's one more file that you'll want to take a look at: *app.module.ts*. In order to view this file, start from the root of your Angular project. From there, the path to the *app.module.ts* file is:

    src > app > app.module.ts

Once you've located this file, open it. You'll notice that the file doesn't have much information present at the moment.

![Image of the app.module.ts file](./images/app-module.PNG)

This file is an example of Angular's modularity system, which is referred to *NgModules*. An *NgModule* is a container for "a cohesive block of code dedicated to an application domain" or "closely related set of capabilities." In other words, an *NgModule* contains metadata about the particular Angular module you're building. This metadata includes information about:
- The components that belong to the module.
- The other modules upon which this module depends for its basic functionality.
- The instructions to Angular's dependency injection system which detail how Angular should determine a value for a needed dependency.
- The primary application, which is titled *AppComponent* by default.

## Step 5: Launch Angular In Development Mode

Now we're finally ready to start developing our Angular application! In order to do so, we'll want to launch Angular in *development mode*. Though doing so isn't necessary, development mode allows developers to much more easily develop Angular applications as this mode allows them to see the changes to their source code reflected in their application in real time.

In order to launch Angular in development mode, we'll use the Angular CLI. Run the following command inside of your Angular project directory:

    ng serve

After running this command, your source code will be compiled. Note that this might take a few moments. If compilation is successful, you'll see the following in your terminal:

![Image of the app.module.ts file](./images/ng-serve.PNG)

You should notice that there is a message which directs the developer to open their browser to "http://localhost:4200". If you follow these instructions, you should see the following in your browser:

![Image of the app.module.ts file](./images/development-mode.PNG)

This view is the result of the existing source code in the project we generated! Angular has a built-in live development server that allows us to view our application in this manner for development purposes. The server runs on port 4200 by default, though you may modify this default behavior by using the `--port` flag.

**Note**: Don't get too used to this view! We'll be replacing it in the next part of this tutorial! That said, have a look at the default view provided to you! It contains links to useful resources that will help you strengthen your knowledge of the Angular framework!

This concludes part one of our Angular tutorial. Please see part 2 of this tutorial if you wish to learn more about Angular components, directives, data binding, and pipes!