# Building a Simple Angular Application: Part Five
---
## Purpose

Part five, the final part of this tutorial, focuses on Angular services and shows developers how to use the HttpClientModule to make HTTP requests from their Angular applications. We'll go into  detail about how to create services, how to inject them into other components, and how to use the HttpClientModule's providers to communicate with other applications.

### Getting Started: Prerequisites

Before you begin the final part of this tutorial, be sure that you've gone through all of the other parts of the tutorial first. Of course, you'll also want to launch your Angular project in development mode, a task you should be extremely comfortable with by now.

## Step 1: Open The File Containing Your Todos-Home Class

We've spent the last couple of portions of this tutorial adding state and functions to our TodosHomeComponent class. It currently looks like this:

```
import { Component, OnInit, Renderer2, ElementRef } from '@angular/core';

@Component({
  selector: 'app-todos-home',
  templateUrl: './todos-home.component.html',
  styleUrls: ['./todos-home.component.css']
})
export class TodosHomeComponent implements OnInit {

  /**
   * Let's add a boolean property called "visibility". We'll make it "true"
   * by default.
   */

  visibility: boolean = true;

  /**
   * This property will track the preferred name the user enters.
   */

  name: string = "";

  /**
   * This property tracks the greeter div's visibility. It has an *initial value of "true" so that the greeter div will be visible
   * when we use *ngIf on it.
   */

  greeterDiv: boolean = true;

  /**
   * Mock Todos. Note that these are simply JavaScript objects.
   */

  todo1 = {
    id: 1,
    content: "Make a cup of coffee before I practice my Java!",
    status: "Incomplete"
  }

  todo2 = {
    id: 2,
    content: "Merge sort all of my socks before laundry day. P.S. I hate laundry days!",
    status: "Incomplete"
  }

  todo3 = {
    id: 3,
    content: "Feed my pet python a snack.",
    status: "Incomplete"
  }

  todo4 = {
    id: 4,
    content: "Have another cup of coffee before I go rehearse the script for my upcoming play: JavaScript.",
    status: "Incomplete"
  }

  todo5 = {
    id: 5,
    content: "Make Kotlin number 1.",
    status: "Complete"
  }

  todo6 = {
    id: 6,
    content: "WFH",
    status: "Complete"
  }

  /**
   * Let's create an array to hold our mock todos!
   */

  todos = [this.todo1, this.todo2, this.todo3, this.todo4, this.todo5, this.todo6];

  constructor() { }

  ngOnInit() {
  }

  /**
   * We're adding an event handler that can be invoked when a user clicks
   * on a button!
   */

  toggleVisibility() {
    this.visibility = !this.visibility;
  }

  submitName() {
    this.greeterDiv = false;
  }

}
```

If you look at our list of todos, you might conclude that our current implementation isn't practical for a couple of reasons:

1. We display the exact same todos each time the user visits the webpage. This means that the view isn't useful to the end user as the data is static.
2. There's no real way to modify our todos as we don't have a way of persisting our todos and any changes that we make to them.

Indeed, this is not how applications work in production. In fact, we've only been going about developing this application in this manner to familiarize with component state and data binding. That said, it's time to revamp our strategy so that our data is pulled from some external source and thus becomes more dynamic.

In order to make this shift, we'll need two additional tools: **services** and the **HttpClientModule**.

## Step 2: Generate Your First Angular Service

Before we generate our first Angular **service**, we should talk about what exactly a service is.

First and foremost, the term *service* is rather broad. It can refer to, for instance, a value, function, or feature that your application needs.

What, then, do we mean in this case when we use the term?

Simply put, an Angular service is usually a class not unlike the classes we've already seen. Unlike the other classes we've seen, however, a service has a narrow purpose; it should have a single responsibility.

That single responsibility is completely up to the developer. Quite often, for example, developers create services which handle making requests for resources hosted on some other server, receiving response payloads, and processing those payloads for later use in their Angular applications. In fact, we will create a small service which performs just this task!

We should also note that Angular services provide a way of making processes reusable across all of our components. Instead of redefining the processes within every component, we can simply define those processes within a service and inject those services into our components.

Now that we're clear on why we use services, let's go ahead generate our first service by using the Angular CLI. Before running the command that follows, you'll want to navigate to your directory of choice as your working directory is where your service will be generated. We have created a directory called "services" in the "app" directory. Once you've completed this task, run this command:

    ng g s <service-name>

Our service will be a service which handles fetching data related to our todos from some external source, so we'll call it "todos". That means that our command will look like this:

    ng g s todos

Once you've generated your service, you should see two new files inside of your services directory: todos.service.spec.ts and todos.service.ts. These files are not conceptually new to you as every component and directive we've generated thus far has contained these files. 

Like directives, services lack templates as they do not require views. This makes sense since we use them to build features that our application needs rather than to build views or handle view-related logic.

In any case, you'll want to open your todos.service.ts file:

![Todos Service](./images/todos-service.PNG)

You'll notice that this service is just a class, and a pretty bare one at that. There is, however, one thing of note: the `@Injectable` decorator. This decorator marks a class as a class that is available to be listed as a provider within our module and injected as a dependency.

But what does this mean?

Something called  **dependency injection** is wired into the Angular framework. Dependency injection allows the framework to provide components with services and other dependencies they might require upon instantiation. In other words, if a component requires a service as a property, the framework will create an instance of that service for you. This is a lot different to the approach we usually take with handling dependencies:

    myDependency:DependencyType = new DependencyType();

The above code has been our typical way of handling dependencies thus far. This method, however, creates tight coupling as changes to my dependency's implementation, for instance, might require changes to the containing class as well. As a result, the source code becomes difficult to maintain and extend.

So how do we inject a dependency for use? It's simple. All you need to do is head over to the component into which you'd like to inject the service. In our case, that's our todos-home component. Once there, make this slight modification to your class' constructor:

```
constructor(private todosService:TodosService) { }
```

And just like that, you've taken advantage of dependency injection. All you have to do know is let the framework handling creating instances of that service as required. You don't even have to think much about the implementation of said service within your TodosHomeComponent class.

## Step 3: Inject An Instance of HttpClient Into Your Service

As we mentioned earlier, we intend to use our Angular service to make HTTP requests for some resource(s) hosted on some server. How can we accomplish this task, however, if we don't have a method making the HTTP requests?

If we were using vanilla JavaScript and weren't working within the confines of a framework, we would use *fetch* or *AJAX*. Technically speaking, we can still take those approaches, but they're not consistent with the way in the framework expects you to handle performing this task.

That said, we're going to rely on a built-in module known as the **HttpClientModule**. This module contains core classes which allow developers to easily make HTTP requests from their Angular applications. Note that this module is just an abstraction over technologies such as *AJAX*.

One class that we're going to rely on in particular from this module is the **HttpClient** class. It is the specific class which handles performing HTTP requests. It is available as an injectable class, so we can inject it into our service like so:

![Injecting Http Client](./images/http-client.PNG)

Now we'll head on our to our app.module.ts file and modify our module a bit:

```
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TodosHomeComponent } from './components/todos-home/todos-home.component';
import { TodosBannerComponent } from './components/todos-banner/todos-banner.component';
import { ColorCodeDirective } from './components/color-code.directive';
import { PunctuationPipe } from './pipes/PunctuationPipe';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    TodosHomeComponent,
    TodosBannerComponent,
    ColorCodeDirective,
    PunctuationPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
```

The only change we've made here is the addition of the HttpClientModule to our imports as this import is external to our project!

## Step 4: Use The HttpClient To Make An HTTP Request

Alright. So we've injected the HttpClient into our service and the HttpClientModule to our imports. Now what?

Well, we should probably add some functions which make HTTP requests. Let's start with a basic `GET` request which fetches all of the todos from a web service that is already hosted somewhere. The endpoint for fetching all of the todos is `34.82.182.44/todos`. So let's add to our service:

```
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Todo } from '../models/Todo';

@Injectable({
  providedIn: 'root'
})
export class TodosService {

  constructor(private httpClient:HttpClient) { }

  getAllTodos():Observable<T>{
     return this.httpClient.get("http://34.82.182.44/todos") as Observable<T>;
  }
}
```

Before you go scratching your head, we know: The above code is not valid; depending on the text editor you're using, the editor might chastise you for that `T` we've used the observable. No worries! We'll be fixing it soon enough. Here are the things we want you to note at this moment:

1. We've defined a function called getAllTodos which returns an `Obervable`. An observable is a representation of a set of values over time. It is a function that can return a stream of values to what is referred to as an "observer", and it may return these values synchronously or asynchronously. In order for an Observable function to begin its execution, you must called the `subscribe` method, which we'll see later.
2. We've used our HttpClient within this function. In this case, we used the `get` method; of course, there are other methods present on the HttpClient class which allow us to make other types of HTTP requests. Notice that the usage of the method is simple. You need only pass the endpoint in as an argument!

Now let us address the generic `T` we used in the above example. As we said before, this `T` causes compilation issues. It does so because it is not a defined type in our Angular project. As it turns out, this `T` represents the type of values that an observable is supposed to emit. This typing is useful as it allows us to grab data asynchronously and define how we intend to use that data throughout our application.

Remember that we are hitting an endpoint which returns todos represented as JSON. We don't, however, want to use JSON within our Angular application. We want "todos". The only problem here is that we haven't strictly defined a "Todo" as a type. In fact, we've just been creating JavaScript objects the entire time. Let us, then, define `Todo` as an official type within our project. We'll do so inside of our app directory, creating a subdirectory called `models`. Inside of that subdirectory, create a new file called `Todo.ts`. Then open that file and type the following:

```
export class Todo{
    id:number;
    title:string;
    createdOn:string;
    completed:boolean;

    constructor(id:number, title:string, createdOn:string, completed:boolean){
        this.id = id; 
        this.title = title; 
        this.createdOn = createdOn;
        this.completed = completed;
    }
}
```

You should be familiar with TypeScript, which means you should know that the above code creates a class that represents a `Todo`. If you're wondering why we chose the field names `id`, `title`, `createdOn`, and `completed`, we did so because we know what the response payload is going to look like after we've made our HTTP request. As it turns out, the data we receive will contain JSON objects those exact properties, so we best follow suit.

After you've defined this class, head back to your service and refactor it like so:

```
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Todo } from '../models/Todo';

@Injectable({
  providedIn: 'root'
})
export class TodosService {

  constructor(private httpClient:HttpClient) { }

  getAllTodos():Observable<Todo[]>{
     return this.httpClient.get("http://34.82.182.44/todos") as Observable<Todo[]>;
  }
}
```

As you can see, we've replaced the `T` with `Todo[]`. This means that our observable will emit an array of our custom type `Todo`. That's perfect as we ultimately want our data represented in this convenient way.

## Step 5: Use Your Injected Service To Pull Data From Your Service Layer Into Your Component

With this modification, we've finished our service method. Now all we need to do is use it! In order to do so, we'll head on over to our todos-home component class. If you remember correctly, we've already injected our newly created service into that component.

```
 constructor(private todosService:TodosService) { }
```

Once you've opened up the todos-home.component.ts file, go ahead and make the following changes:

```
import { Component, OnInit, Renderer2, ElementRef } from '@angular/core';
import { TodosService } from 'src/app/services/todos.service';
import { Observable } from 'rxjs';
import { Todo } from 'src/app/models/Todo';

@Component({
  selector: 'app-todos-home',
  templateUrl: './todos-home.component.html',
  styleUrls: ['./todos-home.component.css']
})
export class TodosHomeComponent implements OnInit {

  /**
   * Let's add a boolean property called "visibility". We'll make it "true"
   * by default.
   */

  visibility: boolean = true;

  /**
   * This property will track the preferred name the user enters.
   */

  name: string = "";

  /**
   * This property tracks the greeter div's visibility. It has an *initial value of "true" so that the greeter div will be visible
   * when we use *ngIf on it.
   */

  greeterDiv: boolean = true;

  /**
   * Let's create an array to hold our mock todos!
   */

  todos:Todo[];

  constructor(private todosService:TodosService) { }

  ngOnInit() {
  }

  /**
   * We're adding an event handler that can be invoked when a user clicks
   * on a button!
   */

  toggleVisibility() {
    this.visibility = !this.visibility;
  }

  submitName() {
    this.greeterDiv = false;
  }

}
```

We've made one change here thus far: We've deleted all of our mock todos! That's because we no longer wish to use mock todos. We instead want to use the todos we retrieve after making an HTTP request to the existing todos API. In order to do that, we have to instead use our service method to populate our todos array. That said, define the following function (`getAllTodos`) within your component class:

```
import { Component, OnInit, Renderer2, ElementRef } from '@angular/core';
import { TodosService } from 'src/app/services/todos.service';
import { Observable } from 'rxjs';
import { Todo } from 'src/app/models/Todo';

@Component({
  selector: 'app-todos-home',
  templateUrl: './todos-home.component.html',
  styleUrls: ['./todos-home.component.css']
})
export class TodosHomeComponent implements OnInit {

  /**
   * Let's add a boolean property called "visibility". We'll make it "true"
   * by default.
   */

  visibility: boolean = true;

  /**
   * This property will track the preferred name the user enters.
   */

  name: string = "";

  /**
   * This property tracks the greeter div's visibility. It has an *initial value of "true" so that the greeter div will be visible
   * when we use *ngIf on it.
   */

  greeterDiv: boolean = true;

  /**
   * Let's create an array to hold our mock todos!
   */

  todos:Todo[];

  constructor(private todosService:TodosService) { }

  getAllTodos(){
    this.todosService.getAllTodos().subscribe(
      data => {
        this.todos = data;
        console.log(this.todos)
      },
      () => {
        console.log("Something went wrong! Can't fetch todos!")
      }
    )
  }

  ngOnInit() {
  }

  /**
   * We're adding an event handler that can be invoked when a user clicks
   * on a button!
   */

  toggleVisibility() {
    this.visibility = !this.visibility;
  }

  submitName() {
    this.greeterDiv = false;
  }

}

```

We'd like to note a couple of things regarding the modified code:

1. We've defined a function called `getAllTodos` on the TodosHomeComponent class. This function is not to be confused with the one we defined on our service class. It's just a function that happens to have the same name.
2. Notice that `getAllTodos` uses our service dependency to call the service layer's `getAllTodos` function; because the service layer's `getAllTodos` returns an observable, we have to call `subscribe` to invoke the observable function.
3. When using `subscribe`, you define two handler functions: one for handling successfully retrieved data, the other for handling any errors which may occur. Our success handler simply takes the retrieved array of todos and makes our `todos` array point to that data. Our error handler simply prints a message to the console.

## Step 6: Implement The `ngOnInit` Method

Even with the addition of our new function, we still haven't achieved the desired functionality we're looking for. If you were to take a look at your todos-home view, you'd see that we still don't have any dynamically generated todos when we load the webpage.

But how do we get them? By taking advantage of Angular's **lifecycle hook interfaces**. Every Angular component has a lifecycle that is managed by the framework. The lifecycle starts when Angular instantiates a component and ends when Angular destroys the component; there are, of course, several lifecycle phases between instantiation and destruction. 

In any case, there's one lifecycle hook that we're interested in at this point: **OnInit**. This hook is associated with the initialization of a component and defines a method for performing tasks when a component's is instantiated. This method is called **ngOnInit**, and it is called once near the beginning of a component's lifecycle.

**Fun Fact**: Many of you probably realize that this method has existed on our component the entire time; we've just been ignoring until now!

We can take advantage of this method by providing a custom implementation of it. In our case, we want to take the data emitted by our observable and store it in our todos array when the component is instantiated. In order to do so, we need to call the `getAllTodos` function we just wrote inside of `ngOnInit`. That said, we'll make the following changes to our component:

```
import { Component, OnInit, Renderer2, ElementRef } from '@angular/core';
import { TodosService } from 'src/app/services/todos.service';
import { Observable } from 'rxjs';
import { Todo } from 'src/app/models/Todo';

@Component({
  selector: 'app-todos-home',
  templateUrl: './todos-home.component.html',
  styleUrls: ['./todos-home.component.css']
})
export class TodosHomeComponent implements OnInit {

  /**
   * Let's add a boolean property called "visibility". We'll make it "true"
   * by default.
   */

  visibility: boolean = true;

  /**
   * This property will track the preferred name the user enters.
   */

  name: string = "";

  /**
   * This property tracks the greeter div's visibility. It has an *initial value of "true" so that the greeter div will be visible
   * when we use *ngIf on it.
   */

  greeterDiv: boolean = true;

  /**
   * Let's create an array to hold our mock todos!
   */

  todos:Todo[];

  constructor(private todosService:TodosService) { }

  getAllTodos(){
    this.todosService.getAllTodos().subscribe(
      data => {
        this.todos = data;
        console.log(this.todos)
      },
      () => {
        console.log("Something went wrong! Can't fetch todos!")
      }
    )
  }

  ngOnInit() {
    this.getAllTodos();
  }

  /**
   * We're adding an event handler that can be invoked when a user clicks
   * on a button!
   */

  toggleVisibility() {
    this.visibility = !this.visibility;
  }

  submitName() {
    this.greeterDiv = false;
  }

}
```

We've only changed one thing: the implementation of `ngOnInit`. Now, whenever the component is instantiated, `getAllTodos` is immediately invoked.

## Step 7: Modify Your Todos-Home Template To Interpolate The Correct Properties

Whew! We've made a lot of changes. Let's take a look at our todos-home view to see what effect we've had on the view:

![New Todos From API](./images/view-props.PNG)

Hmmm... That looks strange. While our todos' `id` properties are interpolated correctly, we seem to be missing the other properties. Well, let's take a look at our template.

```
<div id="greeter">
    <div *ngIf="greeterDiv">
        <label for="yourName">What should we call you? </label>
        <input [(ngModel)]="name" type="text" id="yourName" />
        <button (click)="submitName()">Submit</button>
    </div>
    <h2 *ngIf="name">Welcome, {{name}}.</h2>
</div>
<div class="grid-container">
    <div *ngFor="let todo of todos" [hidden]="visibility" class="todo-card">
        <h2>Todo #{{todo.id}}</h2>
        <p>{{todo.content | uppercase | punctuation}}</p>
        <p appColorCode>Status: {{todo.status}}</p>
        <label for="updateBox{{todo.id}}">Update Task: </label>
        <input [(ngModel)]="todo.content" id="updateBox{{todo.id}}" />
    </div>
</div>
<button (click)="toggleVisibility()">Click Me!</button>
```

Take a look at the properties we're attempting to interpolate here: `id`, `status`, and `content`. Those were the properties of the original JavaScript objects we had created. We're no longer, however, using those objects. We're using data we pulled from an existing API, and while todos still have `id` fields, they no longer have `status` or `content` fields! The new properties for a Todo are: `id`, `title`, `createdOn`, and `completed`.

In other words, we need to update our template so that we can interpolate the right properties! That said, make the following changes:

```
<div id="greeter">
    <div *ngIf="greeterDiv">
        <label for="yourName">What should we call you? </label>
        <input [(ngModel)]="name" type="text" id="yourName" />
        <button (click)="submitName()">Submit</button>
    </div>
    <h2 *ngIf="name">Welcome, {{name}}.</h2>
</div>
<div class="grid-container">
    <div *ngFor="let todo of todos" [hidden]="visibility" class="todo-card">
        <h2>Todo #{{todo.id}}</h2>
        <p>Date: {{todo.createdOn}}</p>
        <p>{{todo.title | uppercase | punctuation}}</p>
        <p appColorCode>Status: {{todo.completed}}</p>
        <label for="updateBox{{todo.id}}">Update Task: </label>
        <input [(ngModel)]="todo.content" id="updateBox{{todo.id}}" />
    </div>
</div>
<button (click)="toggleVisibility()">Click Me!</button>
```

With this final change, your view should display the proper data!

**NOTE**: After you've made all of the changes to your view, you'll want to modify your custom attribute directive as the logic is now outdated! Your attribute directive should now look like this:

![Updated Color Code Directive](./images/color-code-update.PNG)


You've reached the end of our tutorial. We hope you've enjoyed learning Angular and building this simple project with us. We've certainly enjoyed ourselves! In any case, now that you've built a small project, try your hand at building your own Angular application! With the concepts you've learned over the course of this tutorial, you should have no problem doing so! 