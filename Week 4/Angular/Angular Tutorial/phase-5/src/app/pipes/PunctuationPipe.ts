import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: "punctuation"})
export class PunctuationPipe implements PipeTransform{
    transform(value: any, ...args: any[]) {
       let todoContent:string = value;
       if(!todoContent.endsWith(".") && !todoContent.endsWith("!")
       && !todoContent.endsWith("?")){
          value = todoContent.concat(".");
       }
       return value;
    }
}