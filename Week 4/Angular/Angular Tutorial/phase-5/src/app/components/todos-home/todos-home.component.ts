import { Component, OnInit, Renderer2, ElementRef } from '@angular/core';
import { TodosService } from 'src/app/services/todos.service';
import { Observable } from 'rxjs';
import { Todo } from 'src/app/models/Todo';

@Component({
  selector: 'app-todos-home',
  templateUrl: './todos-home.component.html',
  styleUrls: ['./todos-home.component.css']
})
export class TodosHomeComponent implements OnInit {

  /**
   * Let's add a boolean property called "visibility". We'll make it "true"
   * by default.
   */

  visibility: boolean = true;

  /**
   * This property will track the preferred name the user enters.
   */

  name: string = "";

  /**
   * This property tracks the greeter div's visibility. It has an *initial value of "true" so that the greeter div will be visible
   * when we use *ngIf on it.
   */

  greeterDiv: boolean = true;

  /**
   * Let's create an array to hold our mock todos!
   */

  todos:Todo[];

  constructor(private todosService:TodosService) { }

  getAllTodos(){
    this.todosService.getAllTodos().subscribe(
      data => {
        this.todos = data;
        console.log(this.todos)
      },
      () => {
        console.log("Something went wrong! Can't fetch todos!")
      }
    )
  }

  ngOnInit() {
    this.getAllTodos();
  }

  /**
   * We're adding an event handler that can be invoked when a user clicks
   * on a button!
   */

  toggleVisibility() {
    this.visibility = !this.visibility;
  }

  submitName() {
    this.greeterDiv = false;
  }

}
