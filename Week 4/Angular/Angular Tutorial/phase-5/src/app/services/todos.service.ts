import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Todo } from '../models/Todo';

@Injectable({
  providedIn: 'root'
})
export class TodosService {

  constructor(private httpClient:HttpClient) { }

  getAllTodos():Observable<Todo[]>{
     return this.httpClient.get("http://34.82.182.44/todos") as Observable<Todo[]>;
  }
}
