import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TodosHomeComponent } from './components/todos-home/todos-home.component';
import { TodosBannerComponent } from './components/todos-banner/todos-banner.component';
import { ColorCodeDirective } from './components/color-code.directive';
import { PunctuationPipe } from './pipes/PunctuationPipe';

@NgModule({
  declarations: [
    AppComponent,
    TodosHomeComponent,
    TodosBannerComponent,
    ColorCodeDirective,
    PunctuationPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
