# Building a Simple Angular Application: Part Four
---
## Purpose

Part four of this tutorial introduces beginner Angular developers to directives and pipes. During this portion of the tutorial, we'll go into some detail about structural and attribute directives. We'll also play with some built-in Angular pipes and create our own pipes! After completing this portion of the tutorial, you should be able to comfortably use Angular directives to manipulate the DOM and effectively utilize pipes to transform data for presentation purposes.

### Getting Started: Prerequisites

Before you begin the tutorial, please refer to the previous parts of this tutorial as this phase builds on them. You'll also want to know how to launch your Angular project in development mode. We won't cover how to do so in this part of the tutorial since we touched on this in the earlier sections of this tutorial.

## Step 1: Review Your Todo Cards View and Template

Let's once again look through our todos-home component. We'll look at the view and template:

![Current Todos Home View](./images/ngmodel.PNG)

```
<div class="grid-container">
    <div [hidden]="visibility" class="todo-card">
        <h2>Todo #{{todos[0].id}}</h2>
        <p>{{todos[0].content}}</p>
        <p>Status: {{todos[0].status}}</p>
        <label for="updateBox1">Update Task: </label>
        <input [(ngModel)] = "todos[0].content" id="updateBox1"/>
    </div>
    <div [hidden]="visibility" class="todo-card">
        <h2>Todo #{{todos[1].id}}</h2>
        <p>{{todos[1].content}}</p>
        <p>Status: {{todos[1].status}}</p>
        <label for="updateBox2">Update Task: </label>
        <input [(ngModel)] = "todos[1].content" id="updateBox2"/>
    </div>
    <div [hidden]="visibility" class="todo-card">
        <h2>Todo #{{todos[2].id}}</h2>
        <p>{{todos[2].content}}</p>
        <p>Status: {{todos[2].status}}</p>
        <label for="updateBox3">Update Task: </label>
        <input [(ngModel)] = "todos[2].content" id="updateBox3"/>
    </div>
    <div [hidden]="visibility" class="todo-card">
        <h2>Todo #{{todos[3].id}}</h2>
        <p>{{todos[3].content}}</p>
        <p>Status: {{todos[3].status}}</p>
        <label for="updateBox4">Update Task: </label>
        <input [(ngModel)] = "todos[3].content" id="updateBox4"/>
    </div>
</div>
<button (click)="toggleVisibility()">Click Me!</button>
```

In the last part of this tutorial, we used data binding extensively in order to somewhat decouple our content from our view. That said, there are *still* some problems with our template and source code:

1. The template has a lot of redundancy. We essentially have the same `<div>`, for instance, explicitly placed on the page four times. Surely there's a better way to handle placing those cards on the page.
2. The content of the cards still isn't dynamic. The same cards will be loaded every single time we navigate to the webpage.

In this part of the tutorial, we want to tackle the first issue. We're going to do so by using **directives**.

A directive is a marker on an element which allows a specified behavior to be attached that element. This specified behavior can include, for example, removing that element from the DOM under certain circumstances.

There are two types of directives: **structural directives** and **attribute directives**.

A structural directive modifies the DOM's structure by adding, removing, or manipulating elements. Structural directives are preceded by asterisks **(*)**. Examples of structural directives include:

- ***ngIf**
- ***ngFor**

An attribute directive, on the other hand, changes the appearance or behavior of a DOM element. One common example of an attribute directive is **ngStyle**, but we will make our own attribute directive during this tutorial. Notice that the attribute directive name is not preceded by an asterisk.

**NOTE**: We've technically been working with directives all along. As it turns out, components are also directives; they're just directives with templates!

## Step 2: Add A Conditional Greeting Using *ngIf

We'll start by working our structural directives. Let's start by using ***ngIf** to create a conditional greeting for the user! The *ngIf directive removes or adds an element to the DOM based on the value of an expression. If the expression evaluates to "true", the items is placed on the DOM; otherwise, it is removed from the DOM. The syntax for using *ngIf is: `*ngIf="expression"`.

Before we use *ngIf, let's have a look at our todos-home view once again:

![Current Todos Home View](./images/ngmodel.PNG)

We want to personalize this view so that the end user has the option to input a name they'd like to be called by while managing their tasks. In order to accomplish this, we'll first add some state to our component in order to track the user's name. We've only included the snippet that is relevant here as the class is getting quite long!

```
 /**
   * This property will track the preferred name the user enters.
   */

  name:string = "";
```

We'll then add an input box and button which allow the user to do so. The input box will use ngModel for some easy data binding. We'll also add a header which welcomes the user and interpolates the name property we added to the class:

```
<div id="greeter">
    <div>
        <label for="yourName">What should we call you? </label>
        <input [(ngModel)]="name" type="text" id="yourName" />
        <button>Submit</button>
    </div>
    <h2>Welcome, {{name}}.</h2>
</div>
<div class="grid-container">
    <div [hidden]="visibility" class="todo-card">
        <h2>Todo #{{todos[0].id}}</h2>
        <p>{{todos[0].content}}</p>
        <p>Status: {{todos[0].status}}</p>
        <label for="updateBox1">Update Task: </label>
        <input [(ngModel)]="todos[0].content" id="updateBox1" />
    </div>
    <div [hidden]="visibility" class="todo-card">
        <h2>Todo #{{todos[1].id}}</h2>
        <p>{{todos[1].content}}</p>
        <p>Status: {{todos[1].status}}</p>
        <label for="updateBox2">Update Task: </label>
        <input [(ngModel)]="todos[1].content" id="updateBox2" />
    </div>
    <div [hidden]="visibility" class="todo-card">
        <h2>Todo #{{todos[2].id}}</h2>
        <p>{{todos[2].content}}</p>
        <p>Status: {{todos[2].status}}</p>
        <label for="updateBox3">Update Task: </label>
        <input [(ngModel)]="todos[2].content" id="updateBox3" />
    </div>
    <div [hidden]="visibility" class="todo-card">
        <h2>Todo #{{todos[3].id}}</h2>
        <p>{{todos[3].content}}</p>
        <p>Status: {{todos[3].status}}</p>
        <label for="updateBox4">Update Task: </label>
        <input [(ngModel)]="todos[3].content" id="updateBox4" />
    </div>
</div>
<button (click)="toggleVisibility()">Click Me!</button>
```

**NOTE**: As usual, we have modified the stylesheet for this component, but feel free to style your elements as you see fit.

If you take a look at the todos-home view now, you should see the newly added elements. You should also immediately realize that something's a bit odd: The welcome message displays despite the fact that the name property is an empty string. Our *ngIf directive will help us solve this problem, and the fix is simple:

```
<div id="greeter">
    <div>
        <label for="yourName">What should we call you? </label>
        <input [(ngModel)]="name" type="text" id="yourName" />
        <button>Submit</button>
    </div>
    <h2 *ngIf="name">Welcome, {{name}}.</h2>
</div>
<div class="grid-container">
    <div [hidden]="visibility" class="todo-card">
        <h2>Todo #{{todos[0].id}}</h2>
        <p>{{todos[0].content}}</p>
        <p>Status: {{todos[0].status}}</p>
        <label for="updateBox1">Update Task: </label>
        <input [(ngModel)]="todos[0].content" id="updateBox1" />
    </div>
    <div [hidden]="visibility" class="todo-card">
        <h2>Todo #{{todos[1].id}}</h2>
        <p>{{todos[1].content}}</p>
        <p>Status: {{todos[1].status}}</p>
        <label for="updateBox2">Update Task: </label>
        <input [(ngModel)]="todos[1].content" id="updateBox2" />
    </div>
    <div [hidden]="visibility" class="todo-card">
        <h2>Todo #{{todos[2].id}}</h2>
        <p>{{todos[2].content}}</p>
        <p>Status: {{todos[2].status}}</p>
        <label for="updateBox3">Update Task: </label>
        <input [(ngModel)]="todos[2].content" id="updateBox3" />
    </div>
    <div [hidden]="visibility" class="todo-card">
        <h2>Todo #{{todos[3].id}}</h2>
        <p>{{todos[3].content}}</p>
        <p>Status: {{todos[3].status}}</p>
        <label for="updateBox4">Update Task: </label>
        <input [(ngModel)]="todos[3].content" id="updateBox4" />
    </div>
</div>
<button (click)="toggleVisibility()">Click Me!</button>
```

As you can see, we've made a slight modification by adding the *ngIf directive to the `<h2>` element. Now if you take a look at your todos-home view in your browser, you'll notice that you can no longer the welcome message. This is because the expression used with the directive in this case evaluates to false as an empty string evaluates to false.

Of course, we *do* want this message to be visible at some point, and in order to make this happen, we need the component's name property to evaluate to "true".

Because we've ngModel, we can easily update this property's value by simply typing a name into the box. Go ahead. Try it!

![Average Joe](./images/average-joe.PNG)

While this logic works, keeping the input box visible after the user has entered a name isn't very logical. That said, we can also use the *ngIf directive with `<div>` containing the input and button. In order to do so, we'll add some state to the class:

```
/**
   * This property tracks the greeter div's visibility. It has an * initial value of "true" so that the greeter div will be
   * visible when we use *ngIf on it.
   */

  greeterDiv:boolean = true;
```

Now we'll use *ngIf within the template:

```
<div id="greeter">
    <div *ngIf="greeterDiv">
        <label for="yourName">What should we call you? </label>
        <input [(ngModel)]="name" type="text" id="yourName" />
        <button>Submit</button>
    </div>
       <h2 *ngIf="name">Welcome, {{name}}.</h2>
</div>
```

The last piece of the puzzle entails finding some way to change the value of our new state. In other words, we'd like the greeterDiv property we defined to be false after the user hits the "submit" button. Let's use a bit of property binding! We'll first define our handler:

```
submitName(){
     this.greeterDiv = false;
   }
```

Then we'll use the property binding syntax within our template to bind the handler to a click event on our button:

```
<div id="greeter">
    <div *ngIf="greeterDiv">
        <label for="yourName">What should we call you? </label>
        <input [(ngModel)]="name" type="text" id="yourName" />
        <button (click)="submitName()">Submit</button>
    </div>
    <h2 *ngIf="name">Welcome, {{name}}.</h2>
</div>
```

Now if you revisit the view, enter a name, and click the button, you get the following result:

![Updated Average Joe](./images/better-average-joe.PNG)

## Step 3: Use *ngFor To Populate Your View's Todo Cards

Take a good look at our current template:

```
<div id="greeter">
    <div *ngIf="greeterDiv">
        <label for="yourName">What should we call you? </label>
        <input [(ngModel)]="name" type="text" id="yourName" />
        <button (click)="submitName()">Submit</button>
    </div>
    <h2 *ngIf="name">Welcome, {{name}}.</h2>
</div>
<div class="grid-container">
    <div [hidden]="visibility" class="todo-card">
        <h2>Todo #{{todos[0].id}}</h2>
        <p>{{todos[0].content}}</p>
        <p>Status: {{todos[0].status}}</p>
        <label for="updateBox1">Update Task: </label>
        <input [(ngModel)]="todos[0].content" id="updateBox1" />
    </div>
    <div [hidden]="visibility" class="todo-card">
        <h2>Todo #{{todos[1].id}}</h2>
        <p>{{todos[1].content}}</p>
        <p>Status: {{todos[1].status}}</p>
        <label for="updateBox2">Update Task: </label>
        <input [(ngModel)]="todos[1].content" id="updateBox2" />
    </div>
    <div [hidden]="visibility" class="todo-card">
        <h2>Todo #{{todos[2].id}}</h2>
        <p>{{todos[2].content}}</p>
        <p>Status: {{todos[2].status}}</p>
        <label for="updateBox3">Update Task: </label>
        <input [(ngModel)]="todos[2].content" id="updateBox3" />
    </div>
    <div [hidden]="visibility" class="todo-card">
        <h2>Todo #{{todos[3].id}}</h2>
        <p>{{todos[3].content}}</p>
        <p>Status: {{todos[3].status}}</p>
        <label for="updateBox4">Update Task: </label>
        <input [(ngModel)]="todos[3].content" id="updateBox4" />
    </div>
</div>
<button (click)="toggleVisibility()">Click Me!</button>
```

It is completely functional, but we've introduced a bit of redundancy. Each time we wish to define a new card, we explicitly create a new `<div>` and populate it with the contents of a specific.

What if we told you that there was a better way to handle this task? In fact, there is. It's called ***ngFor**.

The *ngFor directive allows you to loop through data. This directive is useful when you have an array of data and you need a simple way of iterating over it to access that data within your template. The syntax for using *ngFor is: `*ngFor="let el of array"`. This syntax should be familiar to you as you also see it in JavaScript!

This particular directive is going to be useful to us as we have an array of todos which we've defined within our class. As an added benefit, we don't have to change anything about our component's state in order to use *ngFor. We only need to make these modifications to our template:

![More Minimal Template](./images/ngfor.PNG)

You'll immediately notice that we've removed many of the elements we explicitly defined within our template. Let's take a look at what exactly is happening here:

1. If you take a look at the `<div>` element which contained all of the todo cards, you'll see that there is only one card defined there now. You should also notice that we've used the *ngFor directive on this element and it's value is "let todo of todos." The use of this directive instructs Angular to loop over each todo in the todos array and generate a `<div>` for every single todo. The `<div>` that is generated for each todo will contain exactly what is present within the single `<div>` element you see above.
2. We've used a fair bit of interpolation within the remaining `<div>` element. You should notice that it uses the loop variable "todo" which we defined in our *ngFor expression. Using this loop variable, we can access a specific todo's data in a very generic way. We don't have to know where this todo exists in the todos array; we only have to know that it exists since *ngFor loops over the entire todos array. The end result is that we get the specific information associated with each todo on each iteration. You should also note that if the size of the todos array changes, the number of cards present in the view will change as well! Cool, right?

To demonstrate point number two, we'll add yet another todo to our todos array:

```
todo5 = {
    id: 5,
    content: "Make Kotlin number 1.",
    status: "Complete"
  }

  /**
   * Let's create an array to hold our mock todos!
   */

  todos = [this.todo1, this.todo2, this.todo3, this.todo4, this.todo5];
  ```

  Aas you can see, we've created just one more todo object and added it to our todos array. This results in the following view:

  ![Five Todo Cards](./images/five-todos.PNG)

## Step 4: Create An Attribute Directive To Color Code Todo Statuses

Now that we have a bit of substance to our application, we need to add a bit of style to it! In order to add that bit of flair, we're going to color code our todo statuses when the user hovers over them with the mouse. Fortunately, Angular allows us to easily handle modifying the style of templates using **attribute directives**. 

As we said earlier, we use attribute directives to modify the appearance or behavior of a DOM element. We want to take advantage of this, so we'll create a custom attribute directive.

Fortunately, we can use the Angular CLI to generate a new directive. Just run the following command in your terminal, making sure that you're inside of your Angular project's directory (and inside of the components directory we created earlier):

    ng generate directive <directive-name>

In our case, we will name our directive "color-code", so our command will look like this:

    ng generate directive color-code

Once you've generated the directive, you should see two files inside of your components folder: color-code.directive.ts and color-code.directive.spec.ts. These file types should be familiar to you as the components we generated earlier had similar files. That's because, as we mentioned earlier, components are actually directives with templates. Because we've generated a simple directive in this case, we don't have a new template or accompanying stylesheet.

In any case, if you open the color-code.directive.ts file, you should see the following:

![Color Code Directive](./images/color-code-directive.PNG)

Take note of the following:

- **the @Directive decorator**: This decorator marks our class as a directive. We can use this directive to attach custom behavior to elements on the DOM using the logic defined within this class.
- **the selector property**: This property specifies the name of the directive. In other words, if we wish to invoke this directive's functionality within our template, we will use `appColorCode` similarly to how we would use `*ngFor` or `*ngIf` within our template.

Now let's define the logic for this directive. As we said before, we would like this directive to color code our cards' statuses. We will make a status of "Incomplete" red and a status of "Complete" green. The user will see these colors when they hover over the status.

Our first step will be to make the following modification to this class:

```
import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[appColorCode]'
})
export class ColorCodeDirective {

  constructor(el: ElementRef) { }

}
```

As you can see, the change was mall. We simply added a parameter to our constructor. Because we have used the "private" keyword with this parameter, it becomes a member of the class. The parameter type, `ElementRef` is a type which allows us to inject a reference to the host DOM element (any element to which our directive is applied within our template).

We'll then write our logic for changing the color of the text:

```
import { Directive, ElementRef, HostListener} from '@angular/core';

@Directive({
  selector: '[appColorCode]'
})
export class ColorCodeDirective {

  constructor(private el: ElementRef) { 
    
  }

 @HostListener('mouseenter') onMouseEnter(){
  if(this.el.nativeElement.innerText === "Status: Incomplete"){
    this.el.nativeElement.style.color = "Crimson";
  } else{
    this.el.nativeElement.style.color = "ForestGreen";
  }
 }
  
}
```

We've added what is called a HostListener here. It is an event listener on the host element which listens for a certain event (mouseenter in this case). Whenever the specified event occurs, this handler function will be invoked. Our handler function simply checks the status of the todo and assigns a color based on the status.

Now we just need to apply our attribute directive to our element of choice. Because we want to modify the element which displays the status of the todos, we will apply to that element using the selector for this directive:

```
<div class="grid-container">
    <div *ngFor="let todo of todos" [hidden]="visibility" class="todo-card">
        <h2>Todo #{{todo.id}}</h2>
        <p>{{todo.content}}</p>
        <p appColorCode>Status: {{todo.status}}</p>
        <label for="updateBox{{todo.id}}">Update Task: </label>
        <input [(ngModel)]="todo.content" id="updateBox{{todo.id}}" />
    </div>
</div>
```

We've applied the `appColorCode` selector to our attribute, so now we're ready to test it out. If you take a look at your todos-home view, the color of the status should change when you hover over the status with your mouse:

We hovered over two of our statuses, one "Complete" and the other "Incomplete," and this is our result:

![Hovering Over Statuses](./images/color-change.PNG)

## Step 5: Create A Pipe That Will Automatically Add Punctuation To Todos

Before we conclude this tutorial, we'd like to discuss one more feature of Angular: **pipes**.

Pipes are used within templates to transform strings, dates, and other types of data for presentation purposes. These transformations do not affect the underlying data, but instead allow us to display that data to the end user in a different form.

Angular already has several built-in pipes. For example, you can use an Angular pipe to transform text to all upper case:

 ```
 <div class="grid-container">
    <div *ngFor="let todo of todos" [hidden]="visibility" class="todo-card">
        <h2>Todo #{{todo.id}}</h2>
        <p>{{todo.content | uppercase}}</p>
        <p appColorCode>Status: {{todo.status}}</p>
        <label for="updateBox{{todo.id}}">Update Task: </label>
        <input [(ngModel)]="todo.content" id="updateBox{{todo.id}}" />
    </div>
</div>
 ```

In the above example, the upper case pipe is applied to the content of the todo. This means that the content will be displayed in all upper case letters in the view:

![Upper Case Tasks](./images/uppercase.PNG)

These built-in pipes can be handy, but we can also create our own pipes. That said, let's attempt to do so.

We'll first create a new directory inside of the "app" folder. We'll call this new directory "pipes". Once you've created this new directory, create a new file inside of this directory called "PunctuationPipe.ts":

![Pipes Folder With New File Within It](./images/pipes.PNG)

Now let's create a class in this file called "PunctuationPipe":

```
export class PunctuationPipe{
    
}
```

After we've created our class, we'll need to use the `@Pipe` decorator, which marks a class as an Angular pipe.

```
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: "punctuation"})
export class PunctuationPipe implements PipeTransform{
    transform(value: any, ...args: any[]) {
        
    }
}
```

The above does the following:

1. It marks the class an Angular pipe that can be used within our template. Note the "name" property of the decorator. This is the name of the pipe (e.g. such as "uppercase") that we use while within our template.
2. It denotes that our class implements the `PipeTransform` interface. This interface mandates that its implementing classes implement the `transform` function. This method takes a "value" parameter which corresponds to the value that it is applied to within the template and any number of other arguments that you wish to pass to the function.

So let's implement the transform method. Remember that our goal is to add punctuation to the tasks on our cards that do not have any punctuation.

```
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: "punctuation"})
export class PunctuationPipe implements PipeTransform{
    transform(value: any, ...args: any[]) {
       let todoContent:string = value;
       if(!todoContent.endsWith(".") && !todoContent.endsWith("!")
       && !todoContent.endsWith("?")){
          value = todoContent.concat(".");
       }
       return value;
    }
}
```

The logic for the `transform` method is simple. We store our value in a string reference so that we can access functions that manipulate strings. We then check that string's contents so that we can verify whether or not ending punctuation exists. If it doesn't exist, we concatenate it to the end of the existing string. In the end, we return our string.

With this logic in place, our pipe is complete. Now we need to do three things.

1. Add our pipe to our app.module.ts file as a declaration.
2. Apply our pipe to our cards' content within the template.
3. Create a new todo (e.g. component state) that does not have punctuation in order to verify that our pipe works.

Here is our app.module.ts file:

![App Module With Pipe Declared](./images/punctuate-module.PNG)

And here is the application of the newly created pipe. Notice that we've used it alongside the uppercase pipe; this is valid as you can chain several pipes to transform data several times. Note that we're using the name of the pipe we defined using the @Pipe decorator.

```
<div id="greeter">
    <div *ngIf="greeterDiv">
        <label for="yourName">What should we call you? </label>
        <input [(ngModel)]="name" type="text" id="yourName" />
        <button (click)="submitName()">Submit</button>
    </div>
    <h2 *ngIf="name">Welcome, {{name}}.</h2>
</div>
<div class="grid-container">
    <div *ngFor="let todo of todos" [hidden]="visibility" class="todo-card">
        <h2>Todo #{{todo.id}}</h2>
        <p>{{todo.content | uppercase | punctuation}}</p>
        <p appColorCode>Status: {{todo.status}}</p>
        <label for="updateBox{{todo.id}}">Update Task: </label>
        <input [(ngModel)]="todo.content" id="updateBox{{todo.id}}" />
    </div>
</div>
<button (click)="toggleVisibility()">Click Me!</button>
```

Now the only thing we have left to do is to modify our component's state:

```
todo6 = {
    id: 6,
    content: "WFH",
    status: "Complete"
  }

  /**
   * Let's create an array to hold our mock todos!
   */

  todos = [this.todo1, this.todo2, this.todo3, this.todo4, this.todo5, this.todo6];
```

We simply created a sixth todo and added it to our todos array. Notice that the sixth todo has no ending punctuation.

After you've completed all of these steps, take a look at your todos-home view. You should see that the pipe has applied punctuation to the new card's content. 

![Updated Todos Home View](./images/grammar.PNG)

We don't know about you, but we always were sticklers for grammar!

This concludes part four of our Angular tutorial. Please see part five of this tutorial to learn how to use Angular services and make HTTP requests within your application.