# Building a Simple Angular Application: Part Two
---
## Purpose

Part two of our Angular tutorial focuses on fundamental Angular concepts such as directives, components, data binding, and pipes. After completing this portion of the tutorial, you should be able to generate Angular components using the Angular CLI, set up Angular routes, use various forms of data binding, utilize built-in pipes, and create custom pipes.

### Getting Started: Prerequisites

Before you begin the tutorial, please refer to part one of this installment. There you'll find information about the basic tools you should familiarize yourself with as well as some introductory information about setting up a basic Angular project.

## Step 1: Open Your Angular Project In Development Mode

In the last part of this tutorial, we launched our Angular project in development mode. If your project is not currently open in development mode, please launch it as we will be making modifications to our project that we will want to see the effects of in real time. Recall that the command for launching your Angular project in development mode is:

    ng serve

Once your project has been compiled, you should open your browser and see the following web page:

![Image of the default home page](./images/development-mode.PNG)

## Step 2: Generate Your First Angular Component

Now that we've launched our project in development mode, let's create our very first Angular **component**. A component is the most basic building block of an Angular application. Components are always associated with a view (or template). Each component's template and logic are separate from that of other components, which makes components fairly modular.

In order to generate your first Angular component, run the following command in your terminal. Make sure that you're inside of your project directory!

    ng generate component <component-name>

You may also use the following alias if you don't wish to type out the full command:

    ng g c <component-name>

We'll name our first component "todos-home", and we'll place it inside of a folder called components. Creating this folder is not necessary, but it will help us organize our components. Our command will look like this:

    ng g c todos-home

**Note:** This is the path to our newly created component if you'd like your folder structure to match ours exactly:

    src > app > components > todos-home

After you've run the command, your terminal should display the following:

![Image of newly generated component folder](./images/todos-home-folder.PNG)

If you take a look inside of your project directory, you should see a new folder called "todos-home" inside of the components folder we created. If you open this folder, you should find four new files:

- todos-home.component.html
- todos-home.component.spec.ts
- todos-home.component.ts
- todos-home.component.css

Each of these files is a vital part of the single component we generated; in fact, each component comes with these files! So what is the importance of each of these files? Well...

- ***.component.html**: This file is the template or view that is associated with the component as the ".html" file extension suggests. This file determines what the end user sees when a component is injected into our **index.html** file.
- ***.component.spec.ts**: We use this file to unit test our component using the JavaScript testing framework *Jasmine*. The file contains a suite and several test cases as well a some test configuration details.
- ***.component.ts**: This file contains the component class where we place all of our component's logic.
- ***.component.css**: This file serves as a style sheet for our component's template.

Because each component has its own versions of these files, a component's logic and style are separate from those of the other components in use in your project. As a result, we can safely make modifications to one component without needing to make drastic changes to another component as a result.

So let's take a look at our first component's template!

![todos-home template](./images/todos-home-template.PNG)

Hmm... This looks a bit bland. No worries! We'll spruce it up later! In the meantime, let's figure out how we can get our end users to view this template!

## Step 3: Set Up Angular Routing

Angular allows developers to create *single page applications*. A single page application is a web application that consists of a single web page that is repeatedly rewritten with new data.

While developing a single page application is possible without using a frontend framework, Angular makes developing single page applications simple. It does so by permitting developers to use what is known as **routing**.

Angular routing allows you to handle "navigation" from one view to the next in a single page application. Because your application is a single page application, you never truly "navigate" to a different view; the framework simply injects the proper view into the single existing web page by stripping out the page's content and writing new data to it.

At this point, some of you might be thinking, "If there's only a single page in an Angular application, what *is* that page and where is it located?" 

This single page is by convention referred to as *index.html*. It is located in the "src" folder which contains all of your application's source code:

![index.html](./images/index.PNG)

Notice that this web page is minimal. It consists of a simple doctype declaration and some basic HTML elements. You should pay spacial attention to the single element contained within the body of the HTML document: app-root.

This element is the selector for the Angular view that is injected into the index.html page. In fact, you can find the view associated with the app-root selector by taking a look at the component that was provided for you when you generated your Angular project: app-component. To be more specific, have a look at the app.component.html file:

![Source code for default view](./images/app-component.PNG)

This file's contents may seem confusing or hard to follow for some of you. After taking a closer look, however, the content should seem familiar to you. That's because it's responsible for generating the current view for your application:

![Image of the default home page](./images/development-mode.PNG)

Of course, we have no need of this default view in our application! We'll replace it with our own views throughout this demo by allowing the framework to inject the views we create into the single page! We'll start by creating a route for our todos-home view!

In order to create our Angular route, we're going to open the *app-routing.module.ts* file that was generated for us when we created our project. You should be able to find this file in the "src" folder with the rest of the source code.

![App routing module](./images/app-routing.PNG)

As you can see, this file doesn't contain much information or source code. You should, however, take note of the following:

- **the array of routes**: This array will contain all of the routes you define in your Angular application. Note that "Route" is a built-in Angular type. A Route consists of a "path" or "URL string" that is associated with a component that will be instantiated when the specified path matches.
- **the NgModule decorator**: A *decorator* is a TypeScript feature which allows developers to annotate or modify classes or class members. Decorators follow the `@expression` form, where `expression` should evaluate to a function that is called at runtime. When called, the function should have access to information about the decorated class or class member. In this case, the NgModule decorator identifies a module's components and defines a process for how to create an injector for a component at runtime. Note the two properties specified within the decorator: *imports* and *exports*. The imports property specifies the "set of NgModules whose exported declarables are available to templates in this module." It also creates a module which contains all of the routes passed to the *forRoot* function. The exports property lists the "set of components, directives, and pipes declared in this NgModule that can be used in the template of any component that is part of an NgModule that imports this NgModule." Note that exported declarations are public and available for use wherever the AppRoutingModule is imported.
- **the AppRoutingModule class**: The AppRoutingModule is simply the class to which all of the NgModule decorator's metadata is applied. The class itself has no properties or functions and is simply used to configure routing in our application.

**NOTE**: If you did not use the CLI to generate your Angular project or are for some reason missing your routing module, you will have to create your routing module on your own. As you can see, however, this task isn't difficult!

Now that we've acquainted ourselves with the routing module, we can start adding our routes! In order to do so, we need only add the individual routes we wish to define in the "routes" array. As of right now, we only have one component with a view that we want to define a route for:

![Routing for todos component](./images/todos-routing.PNG)

We've defined a single route here. The routing object we've defined associates a path (*todos*) with a component (the *todos* component we generated earlier). As a result, whenever the client requests the resource available at `http://localhost:4200/todos`, the template associated with the todos component will be injected into the existing single page. Try requesting that resource in your browser and see what you get!

To many of you, nothing will appear to have changed as you'll still get the view that was generated for you when you created this project. If, however, you scroll down to the bottom of the page, you should see the following:

![Injecting todos home view](./images/todos-home-works.PNG)

Wait! That looks like the template for our todos-home component! But why is located at the bottom of the page? Let's take a look at the template for our app-component. To be more specific, let's take a look at the tail end of the file:

![Router Outlet](./images/router-outlet.PNG)

You should notice a `router-outlet` element at the end of your app-component.html file. The use of this element specifies that you want to inject a component view that is associated with one your existing routes at this location in your app-component template.

In other words, this location is where our todos-home view was injected! That explains why all of the other content present on the page was above the view associated with our custom component.

**NOTE**: If you recall correctly, we indicated that the app-component view was injected into the index.html file's body. In this case, we see that our other components' views are placed inside of our app-component's template. This means that all our component views are not directly injected into the application's single page but indirectly through our app-component!

In any case, we don't want the default view that was generated with our application. That said, all we have to do is update the app-component.html file to remove the elements we don't want to keep:

![Updated App Component Template](./images/new-app-component.PNG)

This change should result in the following view when we type `http://localhost:4200/todos` into the address bar in our browser:

![Updated View](./images/new-todos-home.PNG)

Voila! We have now successfully set up routing for our newly generated todos-home component, though the component view is a tad bit plain. We'll spruce it up later!

Note that you should not delete the `router-outlet` element from your app-component.html file as your component's view will not be injected if you do so!

**NOTE**: You may have noticed that when you type in the default path for accessing your Angular frontend (`http://localhost:4200`) that you now see a blank screen. This is because we removed the default view from our app.component.html file. No worries, however, as we will be adding elements to this homepage as the tutorial progresses!

## Step 4: Modify Your Component's View

Now that we have our routing working, we'll want to modify our component's view as the default view isn't very exciting! Fortunately, modifying the view is simple if you have working knowledge of HTML and CSS!

So where do we modify this view?

If you remember correctly, each component is generated with four files. Of those four files, two will be useful to us now: the template and the stylesheet. In other words, we want to modify the todos-home component's *todos-home.component.html* and *todos-home.component.css* files:

![Todos Template](./images/todos-template.PNG)

![Todos Stylesheet](./images/todos-style.PNG)

In order to place elements in our todos-home view, we must use the above template. Note that we do not have to include a `<head>` or `<body>` tag since this view will be injected into the *index.html* file's body anyway!

In order to modify the style of the elements placed in our view, we use use the above stylesheet!

**NOTE**: Because you should already be familiar with HTML and CSS, we are not going to walk you through the process of designing a web page and styling it. We will, however, show you the finished product!

After modifying our todos-home view, we now have a dashboard that shows all of our pending tasks (i.e. todos)! It's a rather bare bones web page, but we have our todos!

![Todo Cards](./images/todo-cards.PNG)

## Step 5: Create A Banner Component

Hmmm... We've sure left a lot of blank space at the top of the web page! Let's make another component that can fill in some of that space. How about a banner? Let's run `ng g c <component-name>` in our components directory once again, but this time our component name will be "todos-banner". Doing so should generate another component!

After doing so, open the todos-banner.component.html and todos-banner.component.css files so that we can create the view for this component:

![Todos Banner View](./images/todos-banner-html.PNG)

![Todos Banner Style](./images/todos-banner-css.PNG)

Again, we won't be walking you through creating the view as you are already familiar with how to do so.

Here are the finished template and stylesheet:

![Updated Todos Banner View](./images/todos-banner-html-update.PNG)

![Updated Todos Banner Style](./images/todos-banner-css-update.PNG)

Now that we've created and styled our banner, we want to make it visible to the end user. In order to do so, we'll take a look at the todos-banner-component.ts file.

![Todos Banner App Selector](./images/todos-banner-ts.PNG)

Once there, pay special attention to the **@Component decorator**. This decorator supplies metadata about our component. Note the following properties:

- **selector**: This property specifies the CSS selector we should use to place this element on the DOM. In other words, when we create a component, we create reusable view that can be plopped right into another one by simply using the CSS selector. This means that we'll only have to create the view for the banner component once and simply reuse it across our application. Neat!

- **templateUrl**: This property specifies the HTML file which the framework will use to render the view for this component. That is to say, it associates a view with the component. Notice that this property's value is the template we modified earlier.

- **styleUrls**: This property specifies an array of stylesheets that will be used to modify the style of the template associated with this component. Notice that the one stylesheet listed in this array is the stylesheet we modified earlier.

The property that is of most importance to us right now is the **selector** property. That said, take note of this selector. Note that you also have the option to change the selector, but you should remember what value you have chosen for it if you choose to do so.

Now that you have recorded the selector, head over the app-component.html file. Make the following modification:

![Global Banner](./images/app-component-banner.PNG)

All we've done here is place the selector for the banner component with the App Component's view. Recall that the App Component comprises the body of the index.html file. This means that our banner is now a "global" banner that is visible on every page of the application. If you don't believe us, check it out for yourself by navigating to `http://localhost:4200` and `http://localhost:4200/todos`:

![Homepage With Banner](./images/home-sky.PNG)

![Todos Page With Banner](./images/todos-sky.PNG)

**NOTE**: We've also added a background to all of our pages, which is why our view might seem a bit different from yours. We won't go into detail about how we accomplished this task, but you can peek at the styles.css file if you're curious!

## Step 6: Take A Look At Your App Module File

Before we move on to the next part of this tutorial, we want to draw attention to a file that we've overlooked thus far: app.module.ts:

![App Module With Newly Added Modules](./images/app-module-2.PNG)

We briefly talked about this file in the first part of this tutorial, but let's discuss this file as it relates to our current project.

Note the following properties of the @NgModule decorator:

- **declarations**: This property specifies the components, directives, and pipes that belong to this module. In other words, these are the components, directives, and pipes that you've declared in your Angular project. You'll see that all of the components we've generated thus far are listed here.
- **imports**: This property specifies the classes we have imported from other existing modules. Note that these classes aren't classes that we've declared ourselves. They are instead classes that were written by other developers and then used within our application.
- **providers**: This property specifies the creators of services which exist in your application. We have not yet worked with services, but we will revisit this property when we do.
- **bootstrap**: This property specifies the main application view, which is also referred to as the "root component" of the application as it hosts all other app views. In our case, the root component is our AppComponent.

This concludes part two of our Angular tutorial. Please see part three of this tutorial to learn about data binding!