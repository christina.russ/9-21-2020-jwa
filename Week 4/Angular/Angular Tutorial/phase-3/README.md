# Building a Simple Angular Application: Part Three
---
## Purpose

Part three of our Angular tutorial focuses on the types of data binding the Angular frameworks makes available to developers. After completing this portion of the tutorial, you should be familiar the different data binding techniques and know how to use them in a working application.

### Getting Started: Prerequisites

Before you begin the tutorial, please refer to the first two parts of this tutorial as this phase builds on them. You'll also want to know how to launch your Angular project in development mode. We won't cover how to do so in this part of the tutorial since we touched on this in the last two sections.

## Step 1: Take A Look At Your Todo Cards View

Recall that in the last part of this tutorial we created a component for viewing all of our todos. Here are the template and the view corresponding to this component:

![Todos View](./images/todos-sky.PNG)

![Todos Template](./images/todos-home-html.PNG)

Though you're probably excited that you've managed to get your routing working, there is a major problem with our current template: It doesn't allow for dynamically populated todos. In fact, we will only ever see the same todos when this view is injected.

This behavior isn't ideal. We don't, after all, always have the same todos on our lists. Sometimes we want to remove or add tasks.

That said, we need to decouple the todos from the view instead. In other words, we need the contents of our todos to be determined by logic that we place elsewhere in our Angular application. After the contents are determined elsewhere, we can then worry about placing the information on the webpage.

So what's the first step to decoupling from the todos from the template? By using a bit data binding!

What, however, is data binding? Simply put, data binding allows for communication between a view and a component. As a result, when a component's state changes, the view has some way of reflecting those changes. The opposite is also true: when something in the view changes, the component can also receive those updates so that its state can be altered.

There are several types of data binding which are grouped into two larger categories:

- One-Way Data Binding
- Two-Way Data Binding

Commonly used types of one-way data binding include **string interpolation** , **property binding**, and **event binding** while you might frequently see the **NgModel directive** used to perform two-way data binding.

In any case, we won't learn much about data binding until we try it ourselves! That said, let's try our hand at string interpolation in step two!

## Step 2: Set Up Some Property Binding Within The Todos Component

As we said before, the content displayed for the todos view is too rigid. We'd like to make it a bit more dynamic.

Though this part of the tutorial won't completely eliminate that problem, it will take a step towards addressing it. The first step entails using **string interpolation**.

String interpolation allows developers to embed expressions into marked up text. In this example, those expressions will be linked to our component's state. The value of the expressions will depend entirely on our component's state as a result.

In order to use string interpolation, you must use the following syntax within your template: `{{expression}}`.

Before we use this technique within our template, however, we'll need to give our component some state. That said, open up your todos component's todos-home.component.ts file. This file contains our component class and all of the logic associated with it.

![Todos Component Class](./images/todos-home-class.PNG)

So let's our give our component some state! Please place the following code within your todos-home.component.ts file:

```

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-todos-home',
  templateUrl: './todos-home.component.html',
  styleUrls: ['./todos-home.component.css']
})
export class TodosHomeComponent implements OnInit {

todo1 = {
    id: 1,
    content: "Make a cup of coffee before I practice my Java!",
    status: "Incomplete"
  }

  todo2 = {
    id: 2,
    content: "Merge sort all of my socks before laundry day. P.S. I hate laundry days!",
    status: "Incomplete"
  }

  todo3 = {
    id: 3,
    content: "Feed my pet python a snack.",
    status: "Incomplete"
  }

  todo4 = {
    id: 4,
    content: "Have another cup of coffee before I go rehearse the script for my upcoming play: JavaScript.",
    status: "Incomplete"
  }

    todos = [this.todo1, this.todo2, this.todo3, this.todo4];

    constructor() { }

  ngOnInit() {
  }

}

```

Notice that the code which we've added performs a couple of simple tasks:

1. It creates several JavaScript objects with three properties: a numeric *id*, a *content* property of type string, and a *status* property of type string.
2. It also creates an array which contains all of the JavaScript objects where we created.

With these modifications, we've given our component state. Now it's time to make our view communicate with our component. So let's head over to our template and make the following changes:

```
<div class="grid-container">
    <div class="todo-card">
        <h2>Todo #{{todos[0].id}}</h2>
        <p>{{todos[0].content}}</p>
        <p>Status: {{todos[0].status}}</p>
    </div>
    <div class="todo-card">
        <h2>Todo #{{todos[1].id}}</h2>
        <p>{{todos[1].content}}</p>
        <p>Status: {{todos[1].status}}</p>
    </div>
    <div class="todo-card">
        <h2>Todo #{{todos[2].id}}</h2>
        <p>{{todos[2].content}}</p>
        <p>Status: {{todos[2].status}}</p>
    </div>
    <div class="todo-card">
        <h2>Todo #{{todos[3].id}}</h2>
        <p>{{todos[3].content}}</p>
        <p>Status: {{todos[3].status}}</p>
    </div>
</div>

```

The code snippet above performs the following tasks:

1. It completely removes the specific content which existed before. In other words, the information about the todos is pulled from the component class instead of being replaced directly on the template.
2. It accesses the properties of the elements of the todos array we created within our component class using string interpolation. Hence the repeated use of the `{{value}}` syntax.

If you look at the view, you can observe that it hasn't changed; the todos we added to our todos array within the component class had exactly the same content as the content that was initially visible. The only thing that has changed is *where* the data on display comes from.

![Todos View](./images/todos-sky.PNG)

This approach makes the source of our data a bit more dynamic, but we can improve upon it. We will do so at a later point in this tutorial.

In any case, let's now take a look at property binding to see how we can use it to make our webpage more dynamic!

## Step 3: Hide All Of Your Todos Using Property Binding

Let's imagine a scenario in which you didn't want all of your todos visible by default. That is to say, you want to trigger their visibility, likely for design purposes.

In order to hide our todos, we can take advantage of **property binding**.

Property binding entails binding the properties of HTML elements to some value within our component class. In other words, these properties become dependent on the state within our component class. Property binding uses the following syntax: `[prop]="value"`.

So let's start using some property binding! Let's once again take a look at our component class and add the following code snippet. Note that we've added a few comments for clarity since our example is getting longer!

```
@Component({
  selector: 'app-todos-home',
  templateUrl: './todos-home.component.html',
  styleUrls: ['./todos-home.component.css']
})
export class TodosHomeComponent implements OnInit {

  /**
   * Let's add a boolean property called "visibility". We'll make it "true"
   * by default.
   */

   visibility:boolean = true;
  
  /**
   * Mock Todos. Note that these are simply JavaScript objects.
   */

  todo1 = {
    id: 1,
    content: "Make a cup of coffee before I practice my Java!",
    status: "Incomplete"
  }

  todo2 = {
    id: 2,
    content: "Merge sort all of my socks before laundry day. P.S. I hate laundry days!",
    status: "Incomplete"
  }

  todo3 = {
    id: 3,
    content: "Feed my pet python a snack.",
    status: "Incomplete"
  }

  todo4 = {
    id: 4,
    content: "Have another cup of coffee before I go rehearse the script for my upcoming play: JavaScript.",
    status: "Incomplete"
  }

  /**
   * Let's create an array to hold our mock todos!
   */

  todos = [this.todo1, this.todo2, this.todo3, this.todo4];

  constructor() { }

  ngOnInit() {
  }

}
```

Now that we've defined our property, let's head back over to our component's template file and make a few changes:

```
<div class="grid-container">
    <div [hidden]="visibility" class="todo-card">
        <h2>Todo #{{todos[0].id}}</h2>
        <p>{{todos[0].content}}</p>
        <p>Status: {{todos[0].status}}</p>
    </div>
    <div [hidden]="visibility" class="todo-card">
        <h2>Todo #{{todos[1].id}}</h2>
        <p>{{todos[1].content}}</p>
        <p>Status: {{todos[1].status}}</p>
    </div>
    <div [hidden]="visibility" class="todo-card">
        <h2>Todo #{{todos[2].id}}</h2>
        <p>{{todos[2].content}}</p>
        <p>Status: {{todos[2].status}}</p>
    </div>
    <div [hidden]="visibility" class="todo-card">
        <h2>Todo #{{todos[3].id}}</h2>
        <p>{{todos[3].content}}</p>
        <p>Status: {{todos[3].status}}</p>
    </div>
</div>
```

The above modifications perform one simple task: they add a "hidden" attribute to the cards on the webpage. For those of you who are not familiar with the "hidden" attribute, it is used to hide elements on the page. The browser won't render elements which have this attribute set.

Because the value of the "hidden" attribute is "true", the elements are currently hidden. If we were to alter the state of the component and make the value "false", the elements would be perfectly visible.

Now if you navigate to `http://localhost:4200/todos`, you can confirm that there are no todos in sight:

![No Tasks To Complete](./images/no-todos.PNG)

**NOTE**: You're not limited to just hiding elements when using property binding. You can bind any valid property of an element to a value of your choice as long as that value is valid for that property.

But wait! Our todos are now gone, and we don't have a way of getting them back! We need to create some way of making our todos visible once again.

Fear not. Event binding might just be able to get us out of this bind.

## Step 4: Set Up Event Binding To Toggle The Visibility Of Your Todos

But what's **event binding**? Simply put, event binding entails setting up an event handler for a specific event. If, for instance, a user interacts with a certain element on the DOM, a function that a developer has written will be invoked as it is bound to the event that has occurred. This function will be located within our component class. The syntax for using event binding is: `(event) = "handlerCall()"`.

So what is the event that we would like to bind a handler in our case? Well, in the last step we managed to hide our todos, Now, however, there is no way of viewing them when we want to. Perhaps, then, we should set add a button to our page that displays our todos when clicked.

In other words, we need a handler for a click event on a button!

We'll start by adding the handler to our component class:

```
@Component({
  selector: 'app-todos-home',
  templateUrl: './todos-home.component.html',
  styleUrls: ['./todos-home.component.css']
})
export class TodosHomeComponent implements OnInit {

/**
 * Let's add a boolean property called "visibility". We'll make it "true"
 * by default.
 */

  visibility:boolean = true;

  /**
   * Mock Todos. Note that these are simply JavaScript objects.
   */

  todo1 = {
    id: 1,
    content: "Make a cup of coffee before I practice my Java!",
    status: "Incomplete"
  }

  todo2 = {
    id: 2,
    content: "Merge sort all of my socks before laundry day. P.S. I hate laundry days!",
    status: "Incomplete"
  }

  todo3 = {
    id: 3,
    content: "Feed my pet python a snack.",
    status: "Incomplete"
  }

  todo4 = {
    id: 4,
    content: "Have another cup of coffee before I go rehearse the script for my upcoming play: JavaScript.",
    status: "Incomplete"
  }

  /**
   * Let's create an array to hold our mock todos!
   */

  todos = [this.todo1, this.todo2, this.todo3, this.todo4];

  constructor() { }

  ngOnInit() {
  }

  /**
   * We're adding an event handler that can be invoked when a user clicks
   * on a button!
   */

   toggleVisibility(){
    this.visibility = !this.visibility;
   }

}
```

As you can see, we've added a single function called "toggleVisibility". It's a simple function that takes the boolean visibility property and alters it so thats its value is its opposite. In other words, if visibility is "false" when the function is called, it becomes "true" instead.

Now we'll need to actually bind this function to an event if we want to use it as a handler. In order to do so, let's head over to our template and make the following modification:

```
<div class="grid-container">
    <div [hidden]="visibility" class="todo-card">
        <h2>Todo #{{todos[0].id}}</h2>
        <p>{{todos[0].content}}</p>
        <p>Status: {{todos[0].status}}</p>
    </div>
    <div [hidden]="visibility" class="todo-card">
        <h2>Todo #{{todos[1].id}}</h2>
        <p>{{todos[1].content}}</p>
        <p>Status: {{todos[1].status}}</p>
    </div>
    <div [hidden]="visibility" class="todo-card">
        <h2>Todo #{{todos[2].id}}</h2>
        <p>{{todos[2].content}}</p>
        <p>Status: {{todos[2].status}}</p>
    </div>
    <div [hidden]="visibility" class="todo-card">
        <h2>Todo #{{todos[3].id}}</h2>
        <p>{{todos[3].content}}</p>
        <p>Status: {{todos[3].status}}</p>
    </div>
</div>
<button (click)="toggleVisibility()">Click Me!</button>
```

You'll notice that we've made a single change by adding a button with an attribute to the bottom of the file. This addition:

1. Creates a button that displays the text "Click Me!" at its center.
2. Uses the event binding syntax around a "click" event, binding that event to the "toggleVisibility" function we defined. As a result, when this button is clicked, the "toggleVisibility" function will be invoked.

**NOTE**: We applied some styles to our button, which is why it is centered. If you wish to replicate the style, you can view the stylesheet for our todos-home component.

And that's all we need to do to get our event binding working!

That said, let's take a look at our current todos page before clicking the button:

![Newly Added Button](./images/button.PNG)

Now let's see what happens when we click that button:

![After Clicking Button](./images/click.PNG)

As you can see, all of our todos materialize out of thin air! Not only that, but if we click the button again, they disappear again!

## Step 5 : Set Up Two-Way Data Binding To Instantly Update Your Tasks

Thus far, we've worked only with one-way data binding techniques. All of the techniques we used either:

1. Allowed the view to pull data from the model (our class).
2. Allowed the model to respond to events on the view.

In both cases, the communication worked in one direction. Sometimes, however, we want the communication to work in both directions. In such cases, we use two-way data binding.

As we said earlier, a frequently used two-way data binding technique is the **NgModel** directive.

The NgModel directive keeps the view synced with model by tracking user interactions as well as component state. When the state changes or a user interaction occurs, the view and the model communicate with each other so that they may remain in sync. The syntax for using NgModel is: `[(ngModel)]="value"`. This syntax is sometimes playfully referred to as *banana in a box*.

If we want to use NgModel, we'll first have to make a slight addition to our app.module.ts file. NgModel comes from an Angular package that is not included in our project by default. As a result, we must import it ourselves and include the `FormsModule` in our list of imports since this module is external to our project:

![Including the Forms Module](./images/forms-module.PNG)

After you've made this addition, head back over to your template so that you can make some changes there. Here are the changes that you'll need to make:

```
<div class="grid-container">
    <div [hidden]="visibility" class="todo-card">
        <h2>Todo #{{todos[0].id}}</h2>
        <p>{{todos[0].content}}</p>
        <p>Status: {{todos[0].status}}</p>
        <label for="updateBox1">Update Task: </label>
        <input [(ngModel)] = "todos[0].content" id="updateBox1"/>
    </div>
    <div [hidden]="visibility" class="todo-card">
        <h2>Todo #{{todos[1].id}}</h2>
        <p>{{todos[1].content}}</p>
        <p>Status: {{todos[1].status}}</p>
        <label for="updateBox2">Update Task: </label>
        <input [(ngModel)] = "todos[1].content" id="updateBox2"/>
    </div>
    <div [hidden]="visibility" class="todo-card">
        <h2>Todo #{{todos[2].id}}</h2>
        <p>{{todos[2].content}}</p>
        <p>Status: {{todos[2].status}}</p>
        <label for="updateBox3">Update Task: </label>
        <input [(ngModel)] = "todos[2].content" id="updateBox3"/>
    </div>
    <div [hidden]="visibility" class="todo-card">
        <h2>Todo #{{todos[3].id}}</h2>
        <p>{{todos[3].content}}</p>
        <p>Status: {{todos[3].status}}</p>
        <label for="updateBox4">Update Task: </label>
        <input [(ngModel)] = "todos[3].content" id="updateBox4"/>
    </div>
</div>
<button (click)="toggleVisibility()">Click Me!</button>
```

Here are the changes you should note:

1. We've added two elements to each div that represents a card: `<label>` and `<input>`. The label is cosmetic, but the input element will allow us to take user input.
2. The input elements have a special NgModel attribute. This attribute is bound to some property defined within our component class. In our case, we have chosen to bind this attribute to the content property of one of the todos within our todos arrays. Whenever that property's value changes, the content of the input box will change, and whenever the content of the input box changes, the property's value will changed. In other words, there is communication between the model and the view in both directions.

With this setup, our data binding should be working properly. Let's test it out by heading over to the view! Make sure that you click the button to display the cards as our implementation hides the cards by default!

![Using NgModel](./images/ngmodel.PNG)

You should immediately notice that our input boxes are already filled with text. That's because their content is bound to property content of a todo from todos array within our class.

And watch what happens when we change the content of one of the input boxes!

![NgModel Updates View](./images/ngmodel-view-update.PNG)

As you can see, the text inside of the input box wasn't the only thing on the page that changed. The content of the task on the card was simultaneously updated. This trick is useful when we need to modify state and reflect that modification within the view for the benefit of the end user. Neat!

**NOTE**: Of course, none of the changes we make here will be persisted as we are not storing the data that is local to this application anywhere. As a result, your todos will revert to their original state after you've refreshed the page.

This concludes part three of our Angular tutorial. Please see part four of this tutorial to learn all about directives and pipes!