# Topics

> Below you'll find an overview of the topics you should familiarize yourself with for the week. You can also find some resources which should help you familiarize yourself with these topics.

## Java

* Pillars of Object-Oriented Programming
* [Constructors](https://docs.oracle.com/javase/tutorial/java/javaOO/constructors.html)
* Methods and Parameters
* Java 8
   * [Java 8 Information](https://www.java.com/en/download/faq/java8.xml)
   * [Additions to JDK 8](https://www.oracle.com/java/technologies/javase/8-whats-new.html)
* Java Development Kit, Java Runtime Environment, and Java Virtual Machine
   * [Java Garbage Collection and the JVM](https://www.oracle.com/webfolder/technetwork/tutorials/obe/java/gc01/index.html)
* [Keywords](https://docs.oracle.com/javase/tutorial/java/nutsandbolts/_keywords.html)
* [Garbage Collection](https://www.oracle.com/webfolder/technetwork/tutorials/obe/java/gc01/index.html)
* [Arrays](https://docs.oracle.com/javase/tutorial/java/nutsandbolts/arrays.html)
* [Variable Arguments](https://docs.oracle.com/javase/8/docs/technotes/guides/language/varargs.html)
* Packages and Imports
   * [Creating Packages](https://docs.oracle.com/javase/tutorial/java/package/packages.html)
   * [Imports](https://docs.oracle.com/javase/tutorial/java/package/usepkgs.html)
* [Control Statements](https://docs.oracle.com/javase/tutorial/java/nutsandbolts/flow.html)
* [Access Modifiers](https://docs.oracle.com/javase/tutorial/java/javaOO/accesscontrol.html)
* String API
   * [String Class](https://docs.oracle.com/javase/8/docs/api/java/lang/String.html)
   * [Oracle On Strings](https://docs.oracle.com/javase/tutorial/java/data/strings.html)
* StringBuilder and StringBuffer
   * [StringBuilder](https://docs.oracle.com/javase/tutorial/java/data/buffers.html)
   * [StringBuffer](https://docs.oracle.com/javase/8/docs/api/java/lang/StringBuffer.html)
* Exceptions and Exception Handling
   * [Oracle On Exceptions](https://docs.oracle.com/javase/tutorial/essential/exceptions/index.html)
   * [Checked and Unchecked Exceptions](https://docs.oracle.com/javase/tutorial/essential/exceptions/runtime.html)
* Primitive Data Types
   * [Oracle On Primitives](https://docs.oracle.com/javase/tutorial/java/nutsandbolts/datatypes.html)
* Wrapper Classes
* Reflections
   * [Oracle On Reflections](https://www.oracle.com/technical-resources/articles/java/javareflection.html)
* Interfaces and Abstract Classes
   * [Interfaces](https://docs.oracle.com/javase/tutorial/java/concepts/interface.html)
   * [Abstract Classes](https://docs.oracle.com/javase/tutorial/java/IandI/abstract.html)
* File I/O
   * [File Class](https://docs.oracle.com/javase/8/docs/api/java/io/File.html)
   * [FileInputStream](https://docs.oracle.com/javase/8/docs/api/java/io/FileInputStream.html)
   * [FileOutputStream](https://docs.oracle.com/javase/8/docs/api/java/io/FileOutputStream.html)
   * [FileReader](https://docs.oracle.com/javase/8/docs/api/java/io/FileReader.html)
   * [FileWriter](https://docs.oracle.com/javase/8/docs/api/java/io/FileWriter.html)
   * [BufferedReader](https://docs.oracle.com/javase/8/docs/api/java/io/BufferedReader.html)
   * [BufferedWriter](https://docs.oracle.com/javase/8/docs/api/java/io/BufferedWriter.html)
* [Serialization](https://docs.oracle.com/javase/8/docs/technotes/guides/serialization/index.html)
   * [Serializable Interface](https://docs.oracle.com/javase/8/docs/api/java/io/Serializable.html)
* Collections Framework
   * [Collections Class](https://docs.oracle.com/javase/8/docs/api/java/util/Collections.html)
   * [Collection Interface](https://docs.oracle.com/javase/8/docs/api/java/util/Collection.html)
   * [List](https://docs.oracle.com/javase/8/docs/api/java/util/List.html)
   * [Set](https://docs.oracle.com/javase/8/docs/api/java/util/Set.html)
   * [Queue](https://docs.oracle.com/javase/8/docs/api/java/util/Queue.html)
* Object Equality
* Generics
   * [Autoboxing and Unboxing](https://docs.oracle.com/javase/tutorial/java/data/autoboxing.html)
   * [Generic Types](https://docs.oracle.com/javase/tutorial/java/generics/types.html)
* [Object Class](https://docs.oracle.com/javase/8/docs/api/java/lang/Object.html)
* [Threads](https://docs.oracle.com/javase/8/docs/api/java/lang/Thread.html)
   * Synchronization
* Creational Design Patterns
   * Java Bean
   * Singleton
   * Factory
* Lambdas and Functional Interfaces
   * Functional Interfaces
   * [Lambda Expressions](https://docs.oracle.com/javase/tutorial/java/javaOO/lambdaexpressions.html)
* [Annotations](https://docs.oracle.com/javase/tutorial/java/annotations/basics.html)

## Maven

* [Maven Repositories](https://maven.apache.org/guides/introduction/introduction-to-repositories.html)
* [Project Object Model](https://maven.apache.org/guides/introduction/introduction-to-the-pom.html)
* [Default Maven Life Cycle](https://maven.apache.org/guides/introduction/introduction-to-the-lifecycle.html)

## Testing

* [Testing Pyramid](https://www.martinfowler.com/articles/practical-test-pyramid.html)
* [Software Development Life Cycle](https://phoenixnap.com/blog/software-development-life-cycle)
* Software Testing Life Cycle
* [Test-Driven Development](https://www.martinfowler.com/bliki/TestDrivenDevelopment.html)
* Test Case Design
* [JUnit](https://junit.org/junit5/)

## Git

* [What Is Version Control?](https://git-scm.com/video/what-is-version-control)
* [Git Command Line Interface](https://git-scm.com/docs)
* Git Repositories
* Basic Git Workflow
