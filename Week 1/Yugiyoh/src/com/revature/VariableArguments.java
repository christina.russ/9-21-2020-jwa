package com.revature;

public class VariableArguments {

	/*
	 * Why would you want to use variable arguments?
	 * 
	 * Let's imagine that you have a method which adds two numbers together.
	 * 
	 * It looks as if we will have to keep overloading this method if we wish to get
	 * the sum of a different number of integrs. Variable arguments is a solution to
	 * this problem.
	 */

	public int add(int a, int b) {
		return a + b;
	}

	public int add(int a, int b, int c) {
		return a + b + c;
	}

	// Let's write a version of add that takes variable arguments:
	/*
	 * Note that:
	 * 
	 * 1. You can only have one variable argument.
	 * 2. Your variable argument must be the last argument.
	 */
	public int add(/*String b,*/int...a) {
		int sum = 0;
		for (int i : a) {
			sum += i;
		}
		return sum;
	}

	public static void main(String[] args) {

		VariableArguments varArgs = new VariableArguments();
		System.out.println(varArgs.add(3, 4));
		System.out.println(varArgs.add(4, 5, 5));
		System.out.println(varArgs.add(3, 5, 32, 2, 4));
		System.out.println(varArgs.add());
	}

}
