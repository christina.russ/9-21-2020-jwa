package com.revature;

/*
 * The singleton design pattern is used to guarantee that there is only ever
 * a single instance of a class used.
 * 
 * This means that the Singleton should be instantiated once and only once.
 */
public class Singleton {

	//I want to use this as a reference to the one singleton that exists. This
	//is going to point to my singleton.
	private static Singleton singleton;
	
	//keeping track of how many instances are created
	private static int numInstancesCreated;
	
	/*
	 * A singleton has a private constructor. This guarantees that the constructor
	 * is never called elsewhere, meaning that no instances are created elsewhere.
	 */
	private Singleton() {
		
	}
	
	/*
	 * A singleton typically has a getInstance method that returns the same
	 * instance of the class.
	 * 
	 * The "synchronized" keyword makes this method "thread-safe". That does,
	 * however, mean that performance suffers as this runs a bit slower in
	 * a multithreaded environment as each thread must essentially take turns
	 * accessing the method.
	 */
	public static synchronized Singleton getInstance() {
		//We have to make sure that we check that an instance exists.
		//We only use the constructor if no instance exists.
		if(singleton == null) {
			singleton = new Singleton();
			numInstancesCreated++;
		}
		return singleton;
	}
	
	public static void main(String[] args) {
		
		/*
		 * What if I created several new threads that call getInstance()
		 * and started them?
		 */
		for(int i = 0; i < 1000; i++) {
			new Thread2().start();
		}
		
		System.out.println(Singleton.numInstancesCreated);
	}
}

class Thread2 extends Thread{
	
	@Override
	public void run() {
		Singleton.getInstance();
	}
}
