/*
 * Packages allow us to not only separate our concerns, but they allow us to
 * avoid naming conflicts by serving as namespaces.
 */
package com.revature;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.revature.model.GenericCard;
import com.revature.model.MonsterCard;

public class Driver {

	/**
	 * This is the traditional main method in Java.
	 * 
	 * @param args An array of type String.
	 */
	public static void main(String[] args) {

		/*
		 * Thus far, we've been managing each card individually, but keeping track of
		 * all of these references separately becomes increasingly difficult. We need a
		 * way of grouping all of cards together.
		 * 
		 * We're using an array here! Note that arrays are fixed in size. Arrays in Java
		 * are standard in that they have indexes (starting from 0 to size-1).
		 */

		MonsterCard cards[] = new MonsterCard[10];

		System.out.println("Welcome to Yugiyoh!");

		// Let's create some cards!
		MonsterCard darkMagician = new MonsterCard("Dark Magician", 120, true);
		MonsterCard darkMagicianGirl = new MonsterCard("Dark Magician Girl", 100, false);
		MonsterCard darkMagician2 = new MonsterCard("Dark Magician", 70, true);
		/*
		 * When we use a reference to the super type (GenericCard) to point to an
		 * instance of a child class, we refer to this as "covariance".
		 */
		GenericCard generic = new MonsterCard("monster", 120, false);
		
		/*
		 * We want to order our cards based on monster health. The card with
		 * the most health should be first and the card with the least health
		 * should be last based on my implementation of compareTo.
		 */
		
		System.out.println(darkMagicianGirl.compareTo(darkMagician));
		
		/*
		 * As an alternative to using the built-in compareTo method, we can define
		 * another way of comparing our cards. In this case, we want to use
		 * the difficulty level.
		 */
		
		darkMagician.setDifficultyLevel(8);
		darkMagicianGirl.setDifficultyLevel(9);
		System.out.println(new MonsterCardComparator().compare(darkMagician, darkMagicianGirl));
		
		/*
		 * Traditionally, you don't want to create an entirely new class to
		 * define a Comparator. Fortunately, Comparator is a functional
		 * interface, so we can create a lambda expression whenever we need
		 * to define a new Comparator.
		 */
		List<MonsterCard> sampleComparatorList = new ArrayList<>();
		sampleComparatorList.add(darkMagician);
		sampleComparatorList.add(darkMagicianGirl);
		
		//Commented out because we decided to directly pass in the implementation.
		//Comparator<MonsterCard> difficultyComparator = (m1, m2) -> m2.getDifficultyLevel() - m1.getDifficultyLevel();
		
		Collections.sort(sampleComparatorList, (m1, m2) -> m2.getDifficultyLevel() - m1.getDifficultyLevel());
		
		System.out.println("==== Comparator List Output ====");
		System.out.println(sampleComparatorList);

		/*
		 * Let's add all of our cards to our cards array:
		 */

		cards[0] = darkMagician;
		cards[1] = darkMagicianGirl;

		System.out.println("====Printing Available Card Information====");

		/*
		 * This is a traditional for loop. We are using it to iterate through all of the
		 * MonsterCard objects in our array.
		 */
		for (int i = 0; i < cards.length; i++) {
			System.out.println(cards[i]);
		}

		/*
		 * Now that we have a small deck of cards, let's establish a hand for our
		 * player. This hand needs to be dynamic in size, so an array isn't the best
		 * choice right now. Let's choose to work with a Collection implementation.
		 * 
		 * We can impose a type on our collection by using generics. Please note that
		 * generics do not support primitives, but that's okay as we can use Wrapper
		 * classes instead. Wrapper classes provide object representations of primitive
		 * data types.
		 * 
		 * Of course, we can use numbers and other types of primitives with collections
		 * as Java autoboxes them for us.
		 * 
		 * While Set and List are similar in terms of the methods available to
		 * us, note that Queue is a bit different as if offers methods such as:
		 * 
		 * poll
		 * peek
		 * offer
		 * element
		 */

		List<MonsterCard> hand = new ArrayList<>();

		hand.add(darkMagician);
		hand.add(darkMagician);
		hand.add(darkMagicianGirl);
		hand.add(new MonsterCard("Some Dragon", 90, false));
		
		/*
		 * We can have Java sort our hand for us as long as the type contained
		 * within our hand is a Comparable (e.g. implements the Comparable
		 * interface). We'll use the Collections utility class to do this.
		 */
		
		Collections.sort(hand);
		
		System.out.println("Sorted hand:" + hand);
		
		/*
		 * Print the entire hand using an enhanced for loop
		 */
		for (GenericCard card : hand) {
//			if (card instanceof TrapCard) { //Instance of operator is used to check underlying object 
				System.out.println(card);
			//}
		}
		
		/*
		 * Let's create a map of all of the players in our game.
		 * 
		 * When using generics with Maps, we must specify the type for the
		 * key and the value! 
		 * 
		 * Note that you can have null keys in HashMaps! But only one as
		 * all keys in a map must be unique!
		 */
		
		Map<Integer, String> players = new HashMap<>();
		
		players.put(1, "Christina");
		players.put(2, "Fred");
		players.put(3, "Mehrab");
				
		System.out.println(players.containsKey(1));
		System.out.println(players.containsValue("Fred"));
		
		System.out.println(players);
		
		/*
		 * Maps are NOT iterable. Fortunately, sets are iterable, sets
		 * are iterable, and you can grab all of the keys as a set like so:
		 */
		
		for(Integer i : players.keySet()) {
			if(i.intValue() == 2) {
				System.out.println(players.get(i));
			}
		}
		
		/*
		 * Here is the basic syntax for a lambda expression:
		 * 
		 * 1. The type must be that of a functional interface
		 */
		
		CardAttribute difficultyGetter = monster -> monster.getDifficultyLevel();
		CardAttribute healthGetter = (MonsterCard m) -> m.getHealth();
		CardAttribute difficultyGetter2 = (MonsterCard mons) -> {return mons.getDifficultyLevel();};
		CardAttribute healthGetter2 = monsCard -> {System.out.println(monsCard); return monsCard.getHealth();};
		
		LambdaHelper.lambdaHelper(darkMagician, (m1) -> m1.getDifficultyLevel());
	
	/*
	 * How many monster cards have we created?
	 */
		System.out.println(MonsterCard.numCardsCreated);
		System.out.println(darkMagician.numCardsCreated);
	}
}

/*
 * This is a helper class for using our lambda expressions.
 */

class LambdaHelper {

	public static void lambdaHelper(MonsterCard m, CardAttribute c) {
		System.out.println(c.returnAttribute(m));
	}
}