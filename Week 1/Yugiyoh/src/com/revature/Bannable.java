package com.revature;

/*
 * Interfaces are assumed to be public and abstract.
 */
public interface Bannable {
	
	/*
	 * You can have fields on interfaces, but they must be:
	 * 
	 * public
	 * static
	 * final
	 */
	
	public static final boolean BAN_STATUS = false;

	/*
	 * All methods inside of an interface are assumed to be public and abstract.
	 * Note that abstract methods within an interface cannot be final or private.
	 */
	 void goToBanPile();
	 
	 /*
	  * The default keyword allows us to specify a default implementation for
	  * a method on an interface.
	  */
	 default void updateBanStatus() {
		 System.out.println("I will be inherited (with my implementation) by all implementing classes.");
	 }
	 
	 /*
	  * The static keyword also allows us to specify a default implementation
	  * for a method on an interface. Note that this is because you can't have
	  * something that is both static and abstract.
	  */
	 static void printBanStatus() {
		 
	 }

}
