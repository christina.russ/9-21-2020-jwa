package com.revature;

import com.revature.model.MonsterCard;

/*
 * A functional interface is an interface which contains one and only one
 * abstract method.
 * 
 * Note that this does not mean that the interface can't contain default or
 * static methods! Those aren't, after all, abstract methods.
 * 
 * It is good practice to use the @FunctionalInterface annotation to denote
 * that your interface is going to be used as a function interface. It also
 * provides some compile-time safety as the code won't compile if you accidentally
 * add another abstract method to the interface!
 * 
 * Some of the built-in functional interfaces in Java include:
 * 
 * Predicate
 * Consumer
 * Supplier
 * UnaryOperator
 * BinaryOperator
 * Function
 * BiFunction
 * Comparator
 */

@FunctionalInterface
public interface CardAttribute {

	int returnAttribute(MonsterCard m1);
	
	public static void iAmNotAbstract() {
		
	}
}
