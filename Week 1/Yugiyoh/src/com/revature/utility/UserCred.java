package com.revature.utility;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Scanner;

import com.revature.model.MonsterCard;

/*
 * We've created this utility class for taking user input and
 * parsing it.
 */
public class UserCred {

	public static void main(String[] args) {

		/*
		 * It is a common misconception that the Scanner allows us to take user input.
		 * It simply allows us to parse streams of data.
		 * 
		 * In our case, we want to pass System.in to our Scanner as our Scanner wraps
		 * around the stream and parses the data in that stream.
		 */
		Scanner scanner = new Scanner(System.in);

		/*
		 * What if we wanted to allow a user to specify the name of a card? That would
		 * require that we take the user's input.
		 */

		MonsterCard card = new MonsterCard();

		System.out.println("Input the name of your monster card: ");

		/*
		 * We've tried:
		 * 
		 * next() -> This takes the next String, but does not account for spaces
		 * nextLine() -> This takes everything up the newline character.
		 */
		String name = scanner.next(); // reading in suggested name
		scanner.next();

		card.setName(name);

		System.out.println("Input the starting health for this monster: ");

		// Return later.
		// Of course, we should do some checks to be safe.

			int startingHealth = scanner.nextInt();
			card.setHealth(startingHealth);

		System.out.println(card);

		// Always close streams when you're done. But do be careful as when you
		// close your scanner, you're not opening that underlying stream.
		scanner.close();

		/*
		 * Let's say that after you take a user's data, you want to persist. While the
		 * obvious choice for larger applications would be a database, you can use a
		 * file to persists.
		 */

		File file = new File("./userInfo.txt");
		

		/*
		 * When working with files, you'll want some sort of output or input stream for
		 * writing to and reading from the file:
		 * 
		 * FileInputStream -> a stream FileOutputStream -> a stream FileWriter -> wraps
		 * around a stream FileReader -> wraps around a stream BufferedWriter -> wraps
		 * around a writer; is synchronized BufferedReader -> wraps around a reader; is
		 * synchronized
		 */
		
		FileInputStream fis = null;
		FileReader fr = null;
		BufferedReader br = null;
		
		try {
			/*
			 * This is a stream, but you won't directly use it (typically) as it reads in
			 * bytes.
			 */
			fis = new FileInputStream(file);
			/*
			 * Note that the FileReader also takes the file directly, but... it reads
			 * characters. This is still not quite as easy to deal with as entire lines,
			 * etc., but it's a bit better to deal with than a stream of bytes.
			 */
			fr = new FileReader(file);
			/*
			 * But...even so, there's also an alternative to the reader known as the
			 * BufferedReader. This is thread-safe, but a bit slower. Though you trade
			 * performance, you can more easily read data from a stream and parse it.
			 */
			br = new BufferedReader(fr);
			String fileData = br.readLine(); // reads a single line from the file
			System.out.println(fileData);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			/*
			 * This is the ideal place for closing streams and other as it guarantees that
			 * those resources are closed. I will modify this and push the modification.
			 * 
			 * You will all close all of your readers, streams, and buffers!
			 */
			try {
				br.close();
				fr.close();
				fis.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		// So...let's say that we instead want to write to the file now.

		/*
		 * Note: Local variables (variables declared within methods) must be initialized
		 * before being used (etc. calling methods on them or using them in math ops
		 * (e.g. primitives). In order to get around this, I've made these "null" so
		 * that I can use these references later as they're not guaranteed to receive
		 * values in the try-catch block if some exception happens to be thrown.
		 */
		FileOutputStream fos = null;
		FileWriter fw = null;
		BufferedWriter bw = null;

		try {
			fos = new FileOutputStream(file);
			/*
			 * If you pass in a boolean value of "true", you can write to your
			 * file in "append mode", which simply allows you to append to the
			 * end of your file rather than overwriting everything inside of the file
			 * each time.
			 */
			fw = new FileWriter(file, true);
			bw = new BufferedWriter(fw);
			bw.write("A second message!");
			bw.newLine();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			// Closing resources is important for reasons outside of efficiency.
			/*
			 * If you don't close your streams, you don't write to the file.
			 */
			try {
				fos.close();
				bw.close();
				fw.close();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
		}
		
		/*
		 * SERIALIZATION
		 * 
		 * As an extension to this File IO example, we'll discuss serialization.
		 * Serialization entails writing a Java object as Java byte code.
		 * 
		 * As a general note, you won't typically use serialization.
		 */
		
		//Let's serialize an object. In order to serialize an object:
		
		/*
		 * 1) It must be a Serializable. That is it must implement the Serializable
		 * interface. Serializable is a marker interface. It is an empty interface
		 * which is used to mark a class. In this case, Java knows that class that is
		 * marked as Serializable can be serialized.
		 * 
		 * When serializing an object, we use an ObjectOutputStream.
		 */
		
		File serializable = new File("serializable.txt");
		
		/*
		 * We already have a FileOutputStream (we used it earlier), so
		 * we'll use that. An ObjectOutputStream wraps around an OutputStream.
		 * In our case, we are working with files, so we'll use the FileOutputStream.
		 */
		
		ObjectOutputStream oos = null;
		
		try {
			fos = new FileOutputStream(serializable);
			oos = new ObjectOutputStream(fos);
			MonsterCard card2 = new MonsterCard("Lemonade", 19, false);
			oos.writeObject(card2);
		}catch(IOException e) {
			e.printStackTrace();
		}finally {
			try {
				oos.close();
				fos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		//Now let's read the serialized object back in:
		
		ObjectInputStream ois = null;
		
		try {
			fis = new FileInputStream("serializable.txt");
			ois = new ObjectInputStream(fis);
			//Let's deserialize our object.
			MonsterCard card3 = (MonsterCard) ois.readObject();
			System.out.println(card3);
		}catch(ClassNotFoundException e) {
			e.printStackTrace();
		}
		catch(IOException e) {
			e.printStackTrace();
		}finally {
			try {
				ois.close();
				fis.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}
	
}
