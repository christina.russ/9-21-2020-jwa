package com.revature.utility;

public class MonsterNameGenerator {

	public static void main(String[] args) {
		
		String name = "Dark Mage";
		String name2 = new String("Dark Mage");
		
		//Strings are immutable...
		name.concat(" of the Seven Seas");
		
		System.out.println(name);
		
		/*
		 * If we want to work with objects which work "like" mutable Strings, we
		 * can use either StringBuilder or StringBuffer.
		 */
		
		StringBuilder sBuilder = new StringBuilder("Dark Mage");
		System.out.println(sBuilder);
		sBuilder.append(" of the Seven Seas");
		System.out.println(sBuilder);
		
		/*
		 * StringBuffer was the original StringBuilder. StringBuffer is
		 * thread-safe, which makes it safe for use in multithreaded
		 * environments. As a result, it is slower than StringBuilder.
		 */
		StringBuffer sBuffer = new StringBuffer(sBuilder);
		StringBuffer sBuffer2 = new StringBuffer("Dark Mage");
		System.out.println(sBuffer);

	}
}
