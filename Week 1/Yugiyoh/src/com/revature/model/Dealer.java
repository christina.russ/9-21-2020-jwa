package com.revature.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/*
 * Our dealer is more of a utility class than
 * a model.
 */
public class Dealer {

	public static void shuffle(List<GenericCard> deck) {
		
		//Implement your own shuffling algorithm
		Collections.shuffle(deck);
		
		System.out.println("Shuffling...");
	}
	
	public static void main(String...args) {
		
		//Define a deck; it's currently empty
		List<GenericCard> deck = new ArrayList<>();
		
		Player p = new Player();
		
		p.play();
		
		//Eventually, you reach a point at which the delaer must shuffle
		//or reallocate cards for some reason.
		
		Dealer.shuffle(deck);
		
		p.play();
		
		/*
		 * As this is written, a player can play a card, but must then wait
		 * until our Dealer finishes shuffling the deck in order to play
		 * another card. This isn't ideal as we imagine that a player should
		 * be able to continue playing while the dealer shuffles.
		 * 
		 * This is a real-world instance in which modeling with threads might
		 * benefit a developer. A thread is a single line of execution. You might
		 * imagine two main threads running concurrently (NOT simultaneously).
		 * This would allow you to handle multiple tasks concurrently.
		 * 
		 * So how do we create a thread in Java?
		 * 
		 * 1) Extend the Thread class.
		 * 2) Implementing the Runnable interface
		 * 
		 * If you extend the Thread class, you can more directly start() a thread,
		 * but you lose the option to extend your option to extend another class
		 * because Java doesn't support multiple inheritance.
		 * 
		 * If you implement the Runnable interface, you must pass your Runnable
		 * to a Thread constructor, but you keep the option of extending a class.
		 */
		
		//MyThread is a thread. Let's create an instance and start() it.
		
		MyThread myThread = new MyThread();
		myThread.start(); //This starts a thread.
		MyRunnable myRunnable = new MyRunnable();
		//A Runnable needs to be passed to a Thread constructor:
		Thread thread = new Thread(myRunnable);
		thread.start();
		
		
		for(int i = 0; i < 20; i++) {
			System.out.println("This is main.");
		}
	}
}

//Extending the Thread class
class MyThread extends Thread{
	
	/*
	 * The run method defines the logic your thread will carry out.
	 */
	@Override
	public void run() {
		for(int i = 0; i < 20; i++) {
			System.out.println("This is MyThread.");
		}
	}
}

//Implementing the Runnable interface
class MyRunnable implements Runnable{

	@Override
	public void run() {
		for(int i = 0; i < 20; i++)
		System.out.println("My runnable.");
		
	}
	
}
