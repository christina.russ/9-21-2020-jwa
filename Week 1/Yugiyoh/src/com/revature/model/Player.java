package com.revature.model;

import java.util.List;

public class Player {

	private String name;
	private List<GenericCard> hand;
	
	public Player() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Player(String name, List<GenericCard> hand) {
		super();
		this.name = name;
		this.hand = hand;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<GenericCard> getHand() {
		return hand;
	}
	public void setHand(List<GenericCard> hand) {
		this.hand = hand;
	}
	
	/**
	 * This method simulates a user playing a card from
	 * their hand.
	 */
	public void play() {
		System.out.println("Player is playing a card.");
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((hand == null) ? 0 : hand.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Player other = (Player) obj;
		if (hand == null) {
			if (other.hand != null)
				return false;
		} else if (!hand.equals(other.hand))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Player [name=" + name + ", hand=" + hand + "]";
	}
}
