package com.revature.model;

/*
 * An abstract class is a class which may contain abstract methods.
 * Abstract methods do NOT have implementations; the idea is that they will
 * receive their implementations later (in the first concrete class that
 * inherits them).
 * 
 * Note that abstract classes can inherit from other classes.
 */
public abstract class GenericCard {

	protected String name;
	/**
	 * This field tells us whether or not a card is face up or down.
	 */
	protected boolean faceUp;
	//This refers to how hard the card is to use from a user's perspective.
	protected int difficultyLevel;
	
	/*
	 * Abstract methods cannot have implementations!
	 * 
	 * The first concrete class which inherits this abstract method MUST
	 * provide an implementation!
	 */
	public abstract void printDifficultyManual();
	
}
