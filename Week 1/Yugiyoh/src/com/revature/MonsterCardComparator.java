package com.revature;

import java.util.Comparator;

import com.revature.model.MonsterCard;

public class MonsterCardComparator implements Comparator<MonsterCard> {

	@Override
	public int compare(MonsterCard m1, MonsterCard m2) {
		if (m1.getDifficultyLevel() > m2.getDifficultyLevel()) {
			return 1;
		} else if(m1.getDifficultyLevel() < m2.getDifficultyLevel()) {
			return -1;
		}
		return 0;
	}

}
