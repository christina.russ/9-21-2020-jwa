package com.revature.test;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;

import com.revature.model.MonsterCard;

import org.junit.*;

@RunWith(JUnitPlatform.class)
@TestInstance(Lifecycle.PER_CLASS)
public class MonsterCardTest5 {

	private static MonsterCard monsterCard;
	
	@BeforeAll
	public static void setUp() {
		monsterCard = new MonsterCard();
	}
	
	@BeforeEach
	public void perMethodSetUp() {
		
	}
	
	@Test
	public void testInstantiation() {
		Assert.assertNotNull(monsterCard);
	}
	
	@AfterEach
	public void perMethodTearDown() {
		
	}
	
	@AfterAll
	public static void tearDown() {
		
	}
}
