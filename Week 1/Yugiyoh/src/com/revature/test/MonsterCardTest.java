package com.revature.test;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.revature.model.MonsterCard;

/*
 * JUnit is the most popular testing framework for
 * the Java programming language. It is considered easy to use as it
 * is annotation based and requires little to no configuration. Not
 * only that, but the annotations are extremely descriptive.
 */
public class MonsterCardTest {

	//I'm looking to test MonsterCard, so I'll have an instance of MonsterCard
	private static MonsterCard monsterCard;
	
	/*This runs ONCE - before all methods in the class. Anything
	 * that is annotated with BeforeClass needs to be static as
	 * the method will be run before an instance of test class is
	 * created.
	 */
	@BeforeClass
	public static void setUp(){
		monsterCard = new MonsterCard();
	}

	/*
	 * This is run once before every single method. For example,
	 * if there are two tests, this runs twice!
	 */
	@Before
	public void perMethodSetUp() {
		/*
		 * If an operation is very expensive to perform (e.g. it takes
		 * a long time), avoid doing it in a @Before.
		 */
	}
	
	//Denotes that this method is a JUnit test.
	@Test
	@Ignore //ignores a test so that it is not run
	public void testCardInstantiation() {
		Assert.assertNotNull(monsterCard);
	}
	
	@Test
	public void testGoToBanPile() {
		Assert.fail("Not yet implemented"); //tells JUnit to fail this test
	}
	
	/*
	 * This method runs once after every single test.
	 */
	@After
	public void perMethodTearDown() {
		
	}
	
	/*
	 * This method runs ONCE after all tests have run. It is the
	 * very last method that will run.
	 * 
	 * It is usually used to close resources or streams that were
	 * used during testing.
	 */
	@AfterClass
	public static void tearDown() {
		
	}
}
