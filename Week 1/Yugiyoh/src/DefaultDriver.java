
/*
 * Anything in the default package is only accessible
 * within the default package. This class cannot be
 * used in other packages.
 */
public class DefaultDriver {

}
