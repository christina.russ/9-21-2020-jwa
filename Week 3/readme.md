# Topics

> Below you'll find an overview of the topics you should familiarize yourself with for the week. You can also find some resources which should help you familiarize yourself with these topics.

## HTML, CSS

* HTML5
   * [Semantic Tags](https://www.w3schools.com/html/html5_semantic_elements.asp)
* Common HTML Tags
* Element Attributes
* Structure of an HTML Document
* CSS3
   * [Specificity](https://developer.mozilla.org/en-US/docs/Web/CSS/Specificity)
   * Precedence
* [CSS Selectors](https://www.w3schools.com/cssref/css_selectors.asp)
* Including CSS
   * Inline CSS
   * Style Tag
   * External Stylesheet
* [Bootstrap](https://getbootstrap.com/docs/4.5/getting-started/introduction/)

## JavaScript

* What is [JavaScript](https://javascript.info/)?
   * Scripting Languages
   * [ECMAScript](https://en.wikipedia.org/wiki/ECMAScript)
* JSON (JavaScript Object Notation)
* Declaring Variables
   * let
   * var
   * const
* Data Types
* Arrow Notation
* Variable Scopes
* Looping Structures
* Prototypal Inheritance
* *this* keyword
* DOM (Document Object Model)
* DOM Selection
* DOM Manipulation
* Callback Functions
* Event Propagation
   * Bubbling
   * Capturing
* Event Object
* Cancelling Events
* Template Literals
* [AJAX](https://www.w3schools.com/xml/ajax_intro.asp) (Asynchronous JavaScript and XML)
* AJAX Workflow
* XMLHttpRequest

## Client-Server Communication

* [J2EE](https://www.oracle.com/java/technologies/appmodel.html)
* [Tomcat](http://tomcat.apache.org/)

## XML

* What is [XML](https://www.w3schools.com/xml/xml_whatis.asp)
* XML Schema Definition (XSD)
* Document Type Definition (DTD)
* Well-Formed XML
* Valid XML
* XML Namespaces

