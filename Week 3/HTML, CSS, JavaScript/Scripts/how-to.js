let h1 = document.getElementById('theBiggest');
console.log(h1);
h1.setAttribute('hidden', 'true');

//select the email field
let email = document.getElementById('youremail');
//Create autofill for email box.
email.value = 'email@email.com';

//select the password field
let password = document.getElementById('yourpassword');

//Remove the hidden attribute from our h1 element by responding to an event.

//Create a function that is called in response to a certain event.
let func = function unhide(){
    h1.removeAttribute('hidden');
}

//Let's add an event listener to our h2 element:
let h2 = document.getElementById('theSecondBiggest');

//In this case, "func" is a callback function. It is passed to another function and then later invoked within the outer function.
h2.addEventListener('click', func);

//If you use an anonymous function, you can't remove the event as you don't have a reference to that function.
// h2.addEventListener('click', () => { //arrow notation function; arrow functions change the binding of the "this" keyword
//     h1.remove('hidden');
// })

//When events are triggered in JS, the default behavior is "bubbling"; this means that events are propogated from the innermost event to the outermost event.
let header = document.querySelector('header');
console.log(header);

// header.addEventListener('click', () => {
//     window.alert("Congratulations! You've won a free phone!");
// })

let headerh1 = document.querySelector('header > h1');

// headerh1.addEventListener('click', () => {
//     window.alert("Congratulations. You've won a billion dollars!");
// })


//Now we're capturing rather than bubbling.
headerh1.addEventListener('click', () => {
    window.alert("Congratulations! Click here to claim your prize!");
}, true);

header.addEventListener('click', () => {
    window.alert("Congratulations! You're capturing your prize!");
}, true);

//We can also prevent an event from occuring by using the "preventDefault()" function.

let func2 = function cancelEvent(event){
        //Check that you actually can cancel the event.
        if(event.cancelable){
            event.preventDefault();
        }
}

let button = document.getElementById('btn');

button.addEventListener('click', func2);

//Creating new elements

let newEl = document.createElement('p');
newEl.innerHTML = 'this is a new paragraph!';
header.appendChild(newEl);

//Equality In JS

let myVar = 1;
let myVar2 = "1";

//Using ==

console.log(myVar == myVar2); //Just compares value - not data type; uses type coercion; it's a pseudo conversion and it doesn't persist

//Using ===

console.log(myVar === myVar2); //Compares data type and value
