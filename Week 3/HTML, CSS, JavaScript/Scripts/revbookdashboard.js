function grabYugiyoCards(){
    //Select the div inside of which we want to place our cards
    let div = document.querySelector('.card-container');

    //I want to place cards inside of this div. BUT I don't have any; I haven't set up a data source which contains representations of cards, but that's okay because someone else already has. There is (somewhere) a resource available over the network which will provide clients with representations of yugiyo cards (e.g. as JSON). We'll use that resource to grab our cards.

    //Let's use AJAX to accomplish this task!

    //Let's start by creating an XMLHttpRequest object:

    let xhr = new XMLHttpRequest() //readyState 0

    //We need to define what we want to do when the readyState is 4 (meaning that the response body has been populated by the server); in our case, we want to put some cards on the page for the user to see. We can use the onreadystatechange event listener to listen for changes to our readyState. In essence, this listener is invoked each time the readyState changes.

    xhr.onreadystatechange = function(){
        //You decide what to do each time the readyState changes in this function! Be sure to check that the readyState is 4 and that the response code is 200 (meaning everything went smoothly!)

        if(xhr.readyState === 4 & xhr.status === 200){
            //JSON.parse is a convenience function for parsing JSON as JavaScript object
            let cards = JSON.parse(xhr.response)
            for(let i = 0; i < 10; i++){
                console.log(cards.data[i])
                 //Now that we have our cards, we want to place them on the DOM for the end user to see! In order to do so, we'll use divs to represent cards and append those divs to the card container div!

                 let newCard = document.createElement('div')
                 let cardId = document.createElement('h3')
                 let cardName = document.createElement('h3')
                 let cardType = document.createElement('h2')
                 let cardDescription = document.createElement('p')

                 //Let's fill out the text of our newly created elements
                 cardId.innerText = cards.data[i].id
                 cardName.innerText = cards.data[i].name 
                 cardType.innerText = cards.data[i].type 
                 cardDescription.innerText = cards.data[i].desc 

                 //Now that we've filled out the inner contents of our card data, we can append this to a parent div (newCard)

                 newCard.append(cardId)
                 newCard.append(cardName)
                 newCard.append(cardType)
                 newCard.append(cardDescription)

                 //Finally, add my new card to the card container
                 div.append(newCard)
            }

        }
    }

    xhr.open('GET', 'https://db.ygoprodeck.com/api/v7/cardinfo.php') //readyState 1

    xhr.send() //readyState 2
}


//You can take advantage of window's onload event listener which fires immediately after the browser loads the window
window.onload = () => {
    grabYugiyoCards()
}