package com.revature;

import java.util.HashSet;
import java.util.Set;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.revature.model.CardClass;
import com.revature.model.MonsterCard;
import com.revature.model.Realm;
import com.revature.service.MonsterCardService;
import com.revature.utility.HibernateSessionFactory;

public class Driver {


	public static void main(String... args) {
		
		//Dummy Realms for insert
		Realm realm1 = new Realm(1, "Dark Realm");
		Realm realm2 = new Realm(2, "Shadow Realm");
		Realm realm3 = new Realm(3, "Underworld");
		//Dummy card classes for insert
		CardClass c1 = new CardClass(1, "Class 1");
		CardClass c2 = new CardClass(2, "Class 2");
		Set<CardClass> classes = new HashSet<>();
		classes.add(c1);
		classes.add(c2);
		
		MonsterCardService msc = new MonsterCardService();

//		System.out.println(msc.findAllMonsterCards());
		
//		System.out.println(msc.findByName("Feign Client"));
		
		/*
		 * get() vs load()
		 */
		
		Session s = null;
		Transaction tx = null;
		
		try {
			s = HibernateSessionFactory.getSession();
			tx = s.beginTransaction();
			System.out.println(s.load(MonsterCard.class, 22));
//			System.out.println(s.load(MonsterCard.class, 1));
			tx.commit();
		}catch(HibernateException e) {
			e.printStackTrace();
			tx.rollback();
		}finally {
			s.close();
		}
		
		/*
		 * save() vs persist()
		 */
		
		Session s2 = null;
		Transaction tx2 = null;
		MonsterCard myCard = new MonsterCard(100, "Card With Classes", 276, false, realm1, classes);
		MonsterCard canBePersisted = new MonsterCard();
		canBePersisted.setName("Black Mirror Mage");
		
		try {
			s2 = HibernateSessionFactory.getSession();
			tx2 = s2.beginTransaction();
			System.out.println(s2.save(myCard));
//			s2.persist(canBePersisted);
			tx2.commit();
		}catch(HibernateException e) {
			e.printStackTrace();
			tx2.rollback();
		}finally {
			s2.close();
		}
		
		/*
		 * update() vs merge()
		 */
		Session s3 = null;
		Transaction t3 = null;
//		MonsterCard updateMe = new MonsterCard(60, "Winnie The Pooh", 2, true);
		
		try {
			s3 = HibernateSessionFactory.getSession();
			t3 = s3.beginTransaction();
//			s3.update(updateMe);
//			s3.merge(updateMe);
			t3.commit();
		}catch(HibernateException e) {
			e.printStackTrace();
			t3.rollback();
		}finally {
			s3.close();
		}
		
	}
}
