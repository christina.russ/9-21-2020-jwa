package com.revature.controller;

import java.util.List;

import javax.persistence.NonUniqueResultException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.revature.model.MonsterCard;
import com.revature.service.MonsterCardService;

/*
 * Using a Spring controller, we can easily define endpoints and mappings!
 * It's all annotation-driven!
 * 
 * Note that @Controller is a spring stereotype which allows us to mark this
 * class a Spring bean!
 * 
 * The first thing we want to do is define a mapping for this controller.
 * This is the mapping that the HandlerMapping interface will use to determine
 * whether or not this controller should handle a request!
 */

//@Controller(value = "monsterCardController")
/*
 * The RestController annotation is a convenience annotation. It is a combo
 * of the @Controller and @ResponseBody annotations. This means that it is
 * assumed that all methods on this class will write to the Response Body.
 */
@RestController(value = "monsterCardController")
@RequestMapping(value = "/monstercard")
public class MonsterCardController {
	
	private MonsterCardService monsterCardService;
	
	@Autowired
	public void setMonsterCardService(MonsterCardService monsterCardService) {
		this.monsterCardService = monsterCardService;
	}
	
	/*
	 * Let's first use our view resolver to resolve a view! We'll
	 * resolve our index.html file.
	 * 
	 * This method is mapped to "/home" and accepts GET requests!
	 * 
	 * Note that when you are using a standard Spring controller, if you
	 * a return a String, the view resolver will attempt to resolve it as
	 * a view.
	 */
	
	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String goHome() {
		/*
		 * If we're using the internal resource view resolver, when
		 * we return a String, the resolver attempts to resolve that
		 * String as a view.
		 */
		return "index";
	}
	
	/*
	 * We want to write to the response body using Spring. To do this,
	 * we simply need to create a method (and annotate it of course) and
	 * then return the data via the method. You don't even have to
	 * serialize the data yourself!
	 * 
	 * Note that @GetMapping is just a more specific version of @RequestMapping.
	 * 
	 * Also note that the produces attribute specifies what is written to the response
	 * body. This means that whatever this method returns will be written to
	 * the response JSON.
	 * 
	 * While the original version of this method is functional, it's not
	 * a bad idea to send a specific status code to the client via
	 * a ResponseEntity
	 */
	
	@GetMapping(value = "/all", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
	@ResponseBody //specifies that this method writes to the response body
	public ResponseEntity<List<MonsterCard>> findAllMonsterCards(){
		
		return new ResponseEntity<List<MonsterCard>>(
				this.monsterCardService.findAllMonsterCards(),
				HttpStatus.OK);
	}

	/*
	 * Let's play around with a PostMapping!
	 * 
	 * The consumes attribute specifies which media types the client can send
	 * when accessing this resource.
	 * 
	 * The @RequestBody annotation is used with a method parameter. It specifies
	 * that the parameter should be deserialized into a Java object for us
	 * by the framework!
	 */
	
	@PostMapping(value = "/new", consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE})
	public void insertMonsterCard(@RequestBody MonsterCard monsterCard) {
		this.monsterCardService.insert(monsterCard);
	}
	
	/*
	 * Let's write a method which accepts a name from the client and looks for
	 * the monster with that name, then returning said MonsterCard.
	 * 
	 * Using the @PathVariable annotation, we can specify that we want Spring
	 * to take a variable part of our path in as a parameter.
	 */
	
	@GetMapping(value = "/name/{name}", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
	public ResponseEntity<MonsterCard> findMonsterCardByName(@PathVariable String name){
		MonsterCard theCard = this.monsterCardService.findByName(name);
		
		theCard.play();
		
		return new ResponseEntity<MonsterCard>(theCard, HttpStatus.OK);
	}
	
	/*
	 * Let's set up yet another endpoint, this time using a RequestParam!
	 * 
	 * We use the @RequestParam annotation to grab parameters that have been
	 * appended to the URL (e.g. http://example?name=peter)
	 */
	
	@GetMapping(value = "/byname", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
	public ResponseEntity<MonsterCard> findMonsterCardByNameV2(@RequestParam String name){
		MonsterCard theCard = this.monsterCardService.findByName(name);
		
		return new ResponseEntity<MonsterCard>(theCard, HttpStatus.OK);
	}
	
	/*
	 * Spring MVC has several convenient ways of handling Exceptions.
	 * One of those methods is called an ExceptionHandler. It allows us
	 * to define a handler method for certain exceptions!
	 */
	
	@ExceptionHandler(NonUniqueResultException.class)
	public String handleException() {
		return "Something seems to have gone wrong!";
	}
	
}
