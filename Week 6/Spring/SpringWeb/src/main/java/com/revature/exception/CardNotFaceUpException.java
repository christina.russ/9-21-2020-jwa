package com.revature.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.revature.model.MonsterCard;

/*
 * As another method of handling exceptions in Spring Web, you can directly
 * annotate the Exception class itself, specifying a status code to be sent
 * back to the client and a reason.
 */

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Sorry. That MonsterCard name isn't unique.")
public class CardNotFaceUpException extends RuntimeException{

	public static void main(String[] args) {
		
		/*
		 * It is good practice to be specific about the Exception you're
		 * trying to handle.
		 * 
		 * Note that you can only have one try block and that you cannot
		 * use a try block without a catch block or a catch block without
		 * a try block. Of course, you can have as many catch blocks
		 * as you want. Just make sure that your most specific exceptions
		 * are first.
		 * 
		 * You can also optionally have a finally block. A finally block always
		 * executes. As a result, it is often used to close resources such as
		 * connections to databases and streams.
		 */
		try {
			new MonsterCard().play();
		}catch(CardNotFaceUpException e) {
			e.printStackTrace();
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			System.out.println("I always execute.");
		}
		
		/*
		 * This is a RuntimeException that we've created. We do not have to
		 * handle it, but we can.
		 */
		throw new MyRuntimeException();
	}
}

class MyRuntimeException extends RuntimeException{
	
}


