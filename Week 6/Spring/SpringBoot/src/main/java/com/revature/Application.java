package com.revature;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/*
 * @SpringBootApplication marks the main class of a Spring Boot app.
 * 
 * It is a combination of three annotations:
 * 
 * @Configuration
 * @EnableAutoConfiguration
 * @ComponentScan
 * 
 * @EnableAutoConfiguration: allows Spring to automatically determine and
 * autoconfigure beans and add them to the application context
 * 
 * @ComponentScan: provides the behavior of <context:component-scan>
 */
@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}
