package com.revature.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.revature.model.MonsterCard;

/*
 * The Java Persistence API (JPA) provides a standard for implementing basic
 * CRUD methods. Spring Data JPA follows this standard and allows for simple
 * query creation via method names and the JpaRespository.
 * 
 * We're going to extend the JpaRepository in order to take advantage of these
 * pre-implemented methods. The JpaRepository uses generics (the second type is
 * the type of the unique identifiers for your records, the first is the entity
 * type).
 */
public interface MonsterCardRepository extends JpaRepository<MonsterCard, Integer>{

	
	<S extends MonsterCard> S save(MonsterCard monsterCard); 
	List<MonsterCard> findAll();
	void delete(MonsterCard monsterCard);
	MonsterCard findByName(String name);
	/*
	 * Let's do a findAllByPropertyName!
	 * 
	 * We use the @Query annotation to modify the SQL query generated
	 * by the targeted method.
	 */
	@Query(value = "select m from MonsterCard m where m.name = :name")
	List<MonsterCard> findAllByName(@Param("name") String name);
	
	/*
	 * You can use any valid field name in a Spring Data JPA method
	 * name.
	 */
	MonsterCard findById(int id);
	
	
}
