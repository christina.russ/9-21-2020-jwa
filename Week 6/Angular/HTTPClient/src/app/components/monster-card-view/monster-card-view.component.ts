import { Component, OnInit } from '@angular/core';
import { MonsterCardService } from 'src/app/services/monster-card.service';
import {MonsterCard} from 'src/app/models/MonsterCard'
import { Realm } from 'src/app/models/Realm';
import { CardClass } from 'src/app/models/CardClass';

@Component({
  selector: 'app-monster-card-view',
  templateUrl: './monster-card-view.component.html',
  styleUrls: ['./monster-card-view.component.css']
})
export class MonsterCardViewComponent implements OnInit {

  /*
    Let's inject an instance of the MonsterCard service into our MonsterCardView component.
   */
  constructor(private monsterCardService:MonsterCardService) { }

  /*
    This is a component lifecycle hook method. ngOnInit is called when the component is initialized. This works
    similarly to the window's onload property.
   */
  ngOnInit(): void {
    this.getAllMonsterCards()
    this.postMonsterCard()
  }

  //I need an array of MonsterCards
  cards:MonsterCard[] = []
  //Dummy Monster Card to send to the server
  monsterCard:MonsterCard = new MonsterCard('Card of Ages', 1, 87, true, 873, new Realm(1, 'Some realm'), [new CardClass(1, 'Some class')])

  /*
    Note that the HttpClient returns an Observable. You must subscribe to an Observable to access the data in its
    stream. Note that when subscribing, you pass in two callback functions: the first one to handle the handle the
    data that has been extracted, the second one in case of an error.
   */
  getAllMonsterCards(){
    this.monsterCardService.getAllMonsterCards().subscribe(
      (data) => {
        console.log(data)
        this.cards = data
      },
      () => {
        console.log('Something went wrong.')
      }
    )
  }

  postMonsterCard(){
    this.monsterCardService.postMonsterCard(this.monsterCard).subscribe(
      () => {
        console.log('Monster was inserted successfully.')
      },
      () => {
        console.log('Something went wrong!')
      }
    )
  }

}
