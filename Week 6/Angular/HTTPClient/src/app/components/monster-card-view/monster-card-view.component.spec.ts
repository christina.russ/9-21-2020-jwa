import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MonsterCardViewComponent } from './monster-card-view.component';

describe('MonsterCardViewComponent', () => {
  let component: MonsterCardViewComponent;
  let fixture: ComponentFixture<MonsterCardViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MonsterCardViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MonsterCardViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
