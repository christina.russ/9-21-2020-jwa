import { CardClass } from './CardClass';
import { Realm } from './Realm';

export class MonsterCard{
    
    constructor(private name:string, private id:number, private health:number, private faceUp:boolean, private difficultyLevel:number, private realm:Realm, private cardclass:CardClass[] ){
        this.name = name;
        this.id = id;
        this.health - health;
        this.faceUp = faceUp;
        this.realm = realm;
        this.cardclass = cardclass;
        this.difficultyLevel = difficultyLevel;
    }

    getId(){
        return this.id;
    }

    getName(){
        return this.name;
    }

    getHealth(){
        return this.health;
    }

    getFaceup(){
        return this.faceUp;
    }

    getDifficultylevel(){
        return this.difficultyLevel;
    }

    getRealm(){
        return this.realm;
    }

    getCardclass(){
        return this.cardclass;
    }
}