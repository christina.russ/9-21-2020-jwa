import { TestBed } from '@angular/core/testing';

import { MonsterCardService } from './monster-card.service';

describe('MonsterCardService', () => {
  let service: MonsterCardService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MonsterCardService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
