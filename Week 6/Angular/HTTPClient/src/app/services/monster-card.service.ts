import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { Observable } from 'rxjs';
import { MonsterCard } from '../models/MonsterCard';
import { JsonPipe } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class MonsterCardService {

  /*
    We are going to inject an instance of our HttpClient into this service. Recall that Angular also
    uses dependency injection to provide us with with instances of types that we need.
   */
  constructor(private httpClient:HttpClient) { }

  /*
    Angular uses Observables to handle async data.
  */
  getAllMonsterCards():Observable<MonsterCard[]>{
    return this.httpClient.get('http://localhost:8087/monstercard/all') as Observable<MonsterCard[]>
  }

  postMonsterCard(monsterCard:MonsterCard){

    //We must manually construct our HTTP header and Body.
    let headers:HttpHeaders = new HttpHeaders().set('Content-Type', 'application/json');
    let body = JSON.stringify(monsterCard) 
     //Now I actually have to send my request to the server (so make the request).
    return this.httpClient.post('http://localhost:8087/monstercard/new', body, {headers:headers})
  }
}
