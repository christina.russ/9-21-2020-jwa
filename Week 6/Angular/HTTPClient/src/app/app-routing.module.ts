import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MonsterCardViewComponent } from './components/monster-card-view/monster-card-view.component';

const routes: Routes = [
  {
    path: "cards",
    component: MonsterCardViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
