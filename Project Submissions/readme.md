# Project Submissions

> All completed projects should be placed in this directory before the official due date and time. Projects that are NOT placed here will not be marked complete.

## Keep in mind that...

* I do not accept late submissions unless there are extenuating circumstances which I have been notified about before the agreed upon submission date.
* I will not accept "I lost my project" as an excuse. Save your project in multiple places and do take advantage of the fact that you're using version control software which is designed to help combat such issues.
* I do not tolerate plagiarism. Do not steal work from your colleagues or anyone else. There will be consequences.
