# Topics

> Below you'll find an overview of the topics you should familiarize yourself with for the week. You can also find some resources which should help you familiarize yourself with these topics.

## SQL

* [Database Management Systems](https://www.ibm.com/support/knowledgecenter/en/zosbasics/com.ibm.zos.zmiddbmg/zmiddle_46.htm)
   * [Relational Database Management Systems](https://www.codecademy.com/articles/what-is-rdbms-sql)
* [SQL Sublanguages](https://www.w3schools.in/dbms/database-languages/)
   * Data Manipulation Language
   * Data Definition Language
   * Data Query Language
   * Transaction Control Language
   * Data Control Language
* Basic SQL Queries
* [Aggregate Functions](https://www.postgresql.org/docs/9.5/functions-aggregate.html)
* [Scalar Functions](https://www.geeksforgeeks.org/sql-functions-aggregate-scalar-functions/)
* [Multiplicity](https://docs.microsoft.com/en-us/openspecs/sql_data_portability/ms-ssdl/9467f022-7739-46e9-b173-e6a71351661e)
* [Cardinality](https://docs.microsoft.com/en-us/openspecs/sql_data_portability/ms-ssdl/cce4eda9-12e6-40eb-a1d4-5f3d04235cfd#gt_bad829a3-4350-4a42-b6e3-c4f0829a806f)
* [Database Joins](https://www.w3schools.com/sql/sql_join.asp)
* Sub-queries
* [Normalization](https://docs.microsoft.com/en-us/office/troubleshoot/access/database-normalization-description)
   * 0NF
   * 1NF
   * 2NF
   * 3NF
* [Stored Procedures](https://www.postgresql.org/docs/11/sql-createprocedure.html)
* [PL-SQL](https://www.postgresql.org/docs/9.2/plpgsql-overview.html#PLPGSQL-ADVANTAGES)
* [Functions](https://www.postgresql.org/docs/9.2/sql-createfunction.html)
* [Triggers](https://www.postgresql.org/docs/9.2/trigger-definition.html)
* [Cursors](https://www.postgresql.org/docs/9.2/plpgsql-cursors.html)


## JDBC

* [Connection Interface](https://docs.oracle.com/javase/8/docs/api/java/sql/Connection.html)
* [Statement Interface](https://docs.oracle.com/javase/8/docs/api/java/sql/Statement.html)
   * Simple Statements
   * [Prepared Statements](https://docs.oracle.com/javase/8/docs/api/java/sql/PreparedStatement.html)
   * [Callable Statements](https://docs.oracle.com/javase/8/docs/api/java/sql/CallableStatement.html)
* SQL Injection
* Properties File
* Environment Variables

## AWS

* [Benefits of Using AWS](https://aws.amazon.com/application-hosting/benefits/)
* [Relational Database Service (RDS)](https://aws.amazon.com/rds/)
* [Security Groups](https://docs.aws.amazon.com/vpc/latest/userguide/VPC_SecurityGroups.html)
* [AWS Regions](https://aws.amazon.com/about-aws/global-infrastructure/regions_az/)
* [Availabity Zones](https://aws.amazon.com/about-aws/global-infrastructure/regions_az/)

## Testing

* JUnit Review
* Unit Testing
* Mockito
   * Stubbing
   * Mocking

## Design Patterns

* Factory
* Singleton
* Data Access Object (DAO)

# Core Java

* [Threads](https://docs.oracle.com/javase/tutorial/essential/concurrency/runthread.html)
   * States of a Thread
   * [Deadlock](https://docs.oracle.com/javase/tutorial/essential/concurrency/deadlock.html)
   * Creating Threads
   * run() and start()
   * [Thread safety with "synchronized" keyword](https://docs.oracle.com/javase/tutorial/essential/concurrency/sync.html)
* Reflections API
