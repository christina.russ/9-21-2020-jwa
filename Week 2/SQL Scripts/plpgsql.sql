/*
 * PL/pgSQL stands for "Procedural Language/ Postgres Sequential Query Language"
 * 
 * What is it used for? It is used to allow developers to create their own functions,
 * procedures, and triggers. In essence, you can define your own reusable procedures
 * and even specify when they are run (if you're using a trigger).
 */

-- Scalar functions

select game_id, upper(game_name) from game;
select game_id, lower(game_name) from game;
select game_id, concat(game_name, ' is high tier') from game;

-- aggregate functions

select avg(game_id) from game;
select count(game_name) from game;
select sum(game_id) from game;

-- How do we build our own functions? We use pl/pgsql.

-- Let's write a function that checks if a user has a username. In essence, you
-- will pass in the player's firstname and receive data about whether or not they
-- have a non-null username. Note that creating functions, triggers, etc, falls under
-- DDL.

/*
 * In SQL, there are different types of paramters. You have "in" parameters,
 * "out" parameters, and "inout" parameters.
 * 
 * in parameters are parameters that are passed in as arguments when the 
 * function is called
 * 
 * inout parameters are parameters that are passed in and also returned
 * 
 * out paramters: you don't pass these in; you simply return them
 * 
 * In Postgres function definitions are wrapped in "dollar quotes"
 */

drop function hasUsername;
create or replace function hasUsername(in firstname varchar, out username varchar, out usernameExists boolean)
as
$$
	select player_username,
		case when player_username is null then false
		else true
	end
	from player where player_firstname = firstname;
$$ language sql;

-- If you want to use your funtion, you can do the following:

select * from hasUsername('Sue');
select * from hasUsername('Shawn');

-- You can also perform DML inside of a function.

create or replace function insertArchivedGame() returns trigger
language 'plpgsql'
as
$$
begin
	delete from player_game where game_id = old.game_id;
	insert into gamebackup values(default, old.game_name, now());
	return old;
end
$$

/*
 * We've just written a function that removes all references to a game before it is
 * deleted and inserts into a backup table. Now we want to guarantee that this function
 * is always called before a record on our game table is deleted. In essence, we
 * want all of this to automatically when a record is deleted. In order to do this,
 * we can use a trigger! Triggers respond to events on tables, etc.
 */

drop trigger archiveGame on game;
create trigger archiveGame before delete on game
for each row 
execute procedure insertArchivedGame();

select * from player;

select * from gamebackup;