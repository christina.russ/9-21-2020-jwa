-- This file contains DML statements for our card-themed schema

-- Inserting into all of our tables

insert into game values(1, 'Game of Thrones');
insert into game values(2, 'Candyland');
insert into game values(4, 'Spades');
insert into game values(5, 'Polkamans');
insert into game values(8, 'Digimans');
insert into game values(2, 'game of Thrones');  --redundant

insert into player values(1, 'Christina', 'chris');
insert into player values(2, 'Shawn', 'shawn');
insert into player values(3, 'n/a', 'baker');
insert into player values(4, 'Ash', 'Ketchum');
insert into player values(5, 'Sue', null);

insert into card values(1, 'Dark Magician', false, 100);
insert into card values(2, 'Some Dragon', false, 120);
insert into card values(3, 'Furry creature thats cute', false, 20);

insert into player_game values(1, 1);
insert into player_game values(1, 4);
insert into player_game values(3, 1);

/*
 * Updating the records in our table.
 * Make sure that you don't forget your where clause as if you don't have one, you
 * will modify all records in the table!
 */
update player set player_firstname = 'Bash' where player_firstname = 'Ash';

/* If you wish to delete a record, use the "delete" keyword. Again, use a where clause!
 * Christina will come back for that null.
 */

delete from player where player_id = null;

-- Using "is null" keyword to select where a column value is "null"
select * from player where player_username is null;

-- Always commit your DML (assuming you want those changes).
commit;