select * from card;

/*
 * We have the option to select only the colums we need/want. Remember also that
 * double quotes are used to denote columns, though they're optional here.
 */

select "card_id", "card_name" from card;

-- We can order our results based on a column. Note that the default order for
-- order by is ascending. You must specify "desc" for descending order.

select * from player order by player_id;
select * from player order by player_firstname desc;

/*
 * Recall that you can use a "where" clause to filter your results.
 * 
 * The "like" keyword can be used to find, in this example, any firstname
 * which begins with S and contains an "n" at some later point.
 * 
 * We can also use "and" or "or". These keywords work as we would expect them to.
 */
select * from player;
select * from player where player_firstname like 'S%' and player_username like 's%';
select * from player where player_firstname like 'S%' or player_firstname like 'C%';

/*
 * It's possible to query the results of another query. These inner queries are
 * called subqueries.
 * 
 * When you're performing this type of query, it is required that your subquery
 * has an alias. As you can see, the alias provides a simple way of accessing
 * the columns of the inner result set. The keyword for creating an alias is "as".
 */

select * from 
(select * from player where player_firstname like 'S%') 
as innerQuery where innerQuery.player_firstname = 'Shawn';

/*
 * Our player_game table consists entirely of foreign keys. That said, when we
 * view this table, we don't get specific information about players or games
 * without vising both the player and the game table.
 * 
 * We can simplify the process of viewing information about the players and their
 * associated games by using joins.
 */

select * from player_game;

-- This joins allows us to at least see the game_id and the information about the player
-- associated with this game.

select * from player_game inner join player on player_game.player_id = player.player_id;

-- We could also add the game table to this join:

/*
 * Why do we call this an inner join? Well, when you perform an inner join,
 * you get more "complete" records, but what if the foreign key is "null"
 * for a record in your table. Because a null foreign key indicates that there
 * is no relationship between a record on the table containing the "null" and
 * any record on the table you're joining with, these "null" containing records
 * are left out.
 */

select * from player_game 
inner join player on player_game.player_id = player.player_id 
inner join game on player_game.game_id = game.game_id;

/*
 * An outer join, unlike an inner join, will return all records from both tables,
 * but it will substitute "nulls" for those records that couldn't be matched up
 * with each other. In essence, records which have "null" foreign keys will be
 * present, but there won't be a record to match them to, so you'll see "nulls"
 * present where needed.
 */

select * from player_game full join game on player_game.game_id = game.game_id;

/*
 * Left Join: When I say left join, I suggest that there is a left table and a
 * right table. A left join guarantees all of the records from the records from
 * the left table, nut not all of the records from the right table. The only
 * records from the right table which will appear are those that actually match
 * up with a record on my left table.
 */

select * from player_game left join game on player_game.game_id = game.game_id;

/*
 * With a right join, we're guaranteed all of the records from the right table,
 * but we're not guaranteed all of the records from that left table.
 */

select * from  player_game right join game on player_game.game_id = game.game_id;

/*
 * A cross join isn't particularly for this type of application. It returns all
 * of the possible combinations of records from the left table and the right
 * table.
 */
select * from player_game cross join game;

/*
 * Yesterday, we talked about joins. Joins help us combine the data
 * of multiple tables. Now we need to differentiate these from set
   operations. Examples of set operations include:

   union
   union all
   except
   intersect

 * These are all Postgres keywords used to perform set operations.
 * 
 * In order to use set operators, both result sets must have the same number of
 * columns. Not only that...but the data types for the columns which will be
 * stacked on top of each other must match.
 */

(select * from game) union (select "player_id", "player_firstname" from player);

/*
 * Union will combine the results of both queries, but it will omit duplicates.
 * This means that the following union will only display each game once.
 */
(select * from game) union (select * from game);

-- Union all preserves duplicates
(select * from game) union all (select * from game);

-- What about except (also called "minus" in some dialects)? It is useful
-- when you want to view all of the data that is contained in one set but
-- not another.

(select * from game) except (select * from game);

-- intersect: We use this when we want to know if two data sets share any records

(select * from game) intersect (select "game_id", "game_name" from gamebackup);