-- DCL is used to control access to the data within your database. using DCL, you can
-- create users and provide them with passwords and permissions on certain schema.

-- This creates a user called christina with a password of pass. She can login to this database now!
create user christina with password 'pass';

-- Strip christina of all of her privileges. She can do nothing now.
revoke all privileges on schema public from christina;

-- Grant christina permission to access tables on public schema

grant usage on schema public to christina;

-- Grant christina permission to read from the player table. That's it!
grant select on table player to christina;

-- Revoking those same privileges.
revoke usage on schema public from christina;

revoke select on table player from christina;

-- Bye, Christina! Note that in order to drop the user I had to first revoke all
-- privileges as you cannot delete a user that has privileges on objects!
drop user christina;

