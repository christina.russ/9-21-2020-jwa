/* 
 * This is how you write a multiline comment. In any case, we want to model a card
 * game here. As such, we will build tables which can hold information related to
 * a card game.
 */

-- Creating a table to hold game information:

drop table game;
create table game(
	game_id integer primary key,
	game_name varchar unique
);

drop table player;
create table player(
	player_id integer primary key,
	player_firstname varchar not null,
	player_username varchar unique
);

drop table card;
create table card(
	card_id integer primary key,
	card_name varchar unique not null, 
	face_up boolean not null,
	card_health integer not null
);

-- Let's imagine that we want to establish a relationship between games and players
--like so:

/*
 * Players can be involved in multiple games at once, and each game is associate with multiple
 * players.
 */

--Not good for representing this relationship
create table game(
	game_id integer primary key,
	game_name varchar unique,
	player_id integer,
	player2_id integer
);

-- What about this? No. This doesn't work.
create table player(
	player_id integer primary key,
	player_firstname varchar not null,
	player_username varchar unique,
	game_id integer,
);

-- In such situations, it's customary to create what we refer to as a bridge/join
-- table. It looks like this:

create table player_game(
	player_id integer,
	game_id integer
);

/* If you make a mistake in table creation or want to alter a table's structure,
 * use the "alter" keyword. This the preferred way of making changes to tables,
 * especially if they contain records already. Do NOT drop a table that contains
 * records just to modify it in production.
 */

--Altering game_name column data type
alter table game alter column "game_name" set data type text;

--Adding a new column to the game table with the "interval" data type
alter table game add column "game_length" interval;

--Removing that game_length column
alter table game drop column "game_length";
