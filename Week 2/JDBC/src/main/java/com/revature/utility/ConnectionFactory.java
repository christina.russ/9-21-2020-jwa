package com.revature.utility;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/*
 * This class uses a Factory design pattern (though not using types we've defined).
 * The point of using a factory design pattern is that we can return preconfigured,
 * pre-boxed instances on demand.
 */
public class ConnectionFactory {

	private static Connection conn;
	
	/*
	 * I don't want to create instances of my ConnectionFactory just to get JDBC
	 * connections, so I'll just have a static method which returns a new connection
	 * each time it's called.
	 */
	
	public static Connection getConnection() {
		
		try {
			conn = DriverManager.getConnection(
					System.getenv("dburl"),
					System.getenv("dbusername"),
					System.getenv("dbpassword")
					);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return conn;
	}
	
}
