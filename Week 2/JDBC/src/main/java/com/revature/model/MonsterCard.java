package com.revature.model;

import java.io.Serializable;

import com.revature.exception.CardNotFaceUpException;

//This class represents a playing card. This class implicitly extends the Object class.

/*
 * This class is currently a Java Bean. It:
 * 
 * Has all of the features of a POJO (no-args constructor, constructor using
 * fields, getters, and setters).
 * 
 * Overrides key methods from the Object class (toString, hashCode, and equals).
 * 
 * Implements Serializable interface.
 */

// Note that you can only extend a single class in Java.
/*
 * This class now implements the Comparable interface. The comparable interface
 * allows us to define a way of sorting our MonsterCards.
 */
public final class MonsterCard extends GenericCard implements Serializable, Comparable<MonsterCard> {
	
	private static final long serialVersionUID = 1L;
	
	/*
	 * This is a unique identifier for a monster card
	 */
	private int id;
	
	/*
	 * These are the fields for the class. This field is transient,
	 * so it will not be serialized.
	 */
	private transient int health;
	
	/*
	 * The static keyword declares that a member of the class belongs to the
	 * class rather than any one instance.
	 * 
	 * If a member is static, you don't need an instance of the class to access
	 * it. There is only one copy of numCardsCreated. Each instance does NOT
	 * have its own copy.
	 */
	public static int numCardsCreated;
	
	/*
	 * A final reference must always point to the same value or object. In
	 * essence, you can only make an assignment once.
	 * 
	 * If a reference is final, you can make its assignment:
	 * 
	 * 1) As soon as you declare it.
	 * 2) Inside of your constructors.
	 * 3) Inside of an instance initializer.
	 */
	private final int maxNumCards;
	
	/*
	 * This is referred to as an instance initializer. It is a block of code
	 * which is run every single time you create an instance of this class.
	 */
	{
		maxNumCards = 8;
	}

	/*
	 * In Java, if you do not explicitly specify a constructor, Java provides a
	 * default constructor.
	 */

	public MonsterCard() {
		// This a no-args constructor.
		numCardsCreated++;
	}

	public MonsterCard(int id, String name, int health, boolean faceUp) {
		this.id = id;
		this.name = name;
		this.health = health;
		this.faceUp = faceUp;
		this.difficultyLevel = 0;
		numCardsCreated++;
	}
	
	/*
	 * This method simulates a card being played. It can only be
	 * played if it's face up.
	 */
	public void play() throws CardNotFaceUpException {
		if(this.faceUp == false) {
			throw new CardNotFaceUpException();
		}
		System.out.println("Playing this card!");
	}
	
	public int getId() {
		return this.id;
	}
	
	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		if (name.length() > 0) {
			this.name = name;
		} else {
			this.name = "default name";
		}
	}

	public int getHealth() {
		return health;
	}

	public void setHealth(int health) {
		this.health = health;
	}

	public boolean isFaceUp() {
		return faceUp;
	}

	public void setFaceUp(boolean faceUp) {
		this.faceUp = faceUp;
	}
	
	public int getDifficultyLevel() {
		return this.difficultyLevel;
	}
	
	public void setDifficultyLevel(int difficultyLevel) {
		this.difficultyLevel = difficultyLevel;
	}

	@Override
	public String toString() {
		return "Card [name=" + name + ", health=" + health + ", faceUp=" + faceUp + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MonsterCard other = (MonsterCard) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	/*
	 * When we say that MonsterCard is overriding a method, we say that it
	 * is providing a different implementation for a method that it has
	 * inherited.
	 */
	@Override
	public void printDifficultyManual() {
		System.out.println("Not yet implemented");
	}
	
	/*
	 * This is a method overload. Note that you must change either:
	 * 
	 * 1. The number of parameters.
	 * 2. The types of the parameters.
	 * 
	 * Of course you can have as many overloads as you want.
	 * 
	 * Note that this method is now final. This means that any class which
	 * inherits it cannot override it. It can, however, still be overloaded
	 * in this class as you see below!
	 */
	public final void printDifficultyManual(String instructions) {
		
	}
	
	public void printDifficultyManual(String instructions, String creator) {
		
	}

	/*
	 * I must define my criteria for sorting MonsterCards within this method!
	 */
	@Override
	public int compareTo(MonsterCard o) {
		
		if(this.health < o.health) {
			return -1;
		} else if(this.health > o.health){
			return 1;
		}
		return 0;
	}

}

/*
 * We are NOT allowed to extend a final class.
 */
class MageCard /*extends MonsterCard*/{
	
}