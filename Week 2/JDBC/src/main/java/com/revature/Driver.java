package com.revature;

import com.revature.model.MonsterCard;
import com.revature.service.MonsterCardService;

public class Driver {

	public static void main(String[] args) {
		
		// MonsterCardService used to access service layer methods
		MonsterCardService mcs = new MonsterCardService();
				
		System.out.println(mcs.findAllMonsterCards());
		
		System.out.println(mcs.findAllMonsterCardsSorted((c, c2) -> c.getHealth() - c2.getHealth()));
				
		/*
		 * My user input is valid SQL. Using a simple Statement, this user input
		 * would unfortunately be executed as SQL.
		 * 
		 * NOTE: As a general note, there now seems to be a feature of JDBC that is
		 * preserving this table and rolling back commits if an exception is thrown.
		 * 
		 * I will actually rework this example so that there is no exception thrown and
		 * compare the behavior of both.
		 * 
		 * SECOND NOTE: Josh put comments lines in his SQL statement to get the drop to 
		 * be effective. :)
		 */
		String userInput = "Alucard' where card_name = 'Nosferatu'; drop table card; --" +
		" update card set card_name = 'any name";
		
		//Let's create a MonsterCard that we can pass to our update method.
		
		MonsterCard monsterCard = new MonsterCard(1, userInput, 22, false);
		
		mcs.update(monsterCard);
	}
}
