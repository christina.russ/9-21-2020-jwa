package com.revature.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.revature.model.MonsterCard;
import com.revature.utility.ConnectionCloser;
import com.revature.utility.ConnectionFactory;

public class MonsterCardRepositoryImpl implements MonsterCardRepository {

	// Let's try a basic insert!
	@Override
	public void insert(MonsterCard monsterCard) {

		/*
		 * Recall that JDBC has several core classes and interfaces. These are all used
		 * in your JDBC workflow.
		 */

		Connection conn = null;
		Statement stmt = null;

		try {
			/*
			 * The first step is to establish a connection to our database.
			 */
			conn = ConnectionFactory.getConnection();
			/*
			 * Now I need to write a statement that would perform the insert.
			 * This should be a SQL statement. In order to do this using JDBC,
			 * I'll need to use the Statement interface.
			 */
			
			stmt = conn.createStatement();
			
			/*
			 * Using my statement object, I can then prepare a SQL statement
			 * to be later executed.
			 */
			
			stmt.execute(
					"insert into card values("
							+ monsterCard.getId() + ", '" 
							+ monsterCard.getName() + "', " 
							+ monsterCard.getHealth() + ", " 
							+ monsterCard.isFaceUp() + ")");

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ConnectionCloser.closeResource(conn);
			ConnectionCloser.closeResource(stmt);
		}
	}

	@Override
	public List<MonsterCard> findAllMonsterCards() {
		
		List<MonsterCard> cards = new ArrayList<>();
		/*
		 * Let's try to get a list of all of our cards!
		 */
		
		//We need a connection. We also know that we need a statement.
		//We also need a ResultSet in this case because we need to store the results of our query.
		
		Connection conn = null;
		Statement stmt = null;
		ResultSet set = null;
		final String SQL = "select * from card";
		
		try {
			conn = ConnectionFactory.getConnection();
			stmt = conn.createStatement();
			set = stmt.executeQuery(SQL);
			
			/*
			 * We have a populated ResultSet, but we need to take the records from that
			 * ResultSet and store them in a List.
			 */
			
			while(set.next()) {
				cards.add(
						new MonsterCard(
								set.getInt(1),
								set.getString(2),
								set.getInt(3),
								set.getBoolean(4))
						);
			}
			
			/*Do NOT perform business logic in your repository. This is the data
			 access layer of our application (this follows the DAO design pattern).
			 The only purpose of this layer of your application is to communicate 
			 with the database.
			 
			 If, for instance, I find all of the cards, and then I start modifying
			 the data contained with my list of cards, I'm committing to always
			 using those cards with those modifications each time I use this method.
			 */
			
			
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			ConnectionCloser.closeResource(conn);
			ConnectionCloser.closeResource(stmt);
			ConnectionCloser.closeResource(set);
		}
		
		return cards;
	}

	@Override
	public void update(MonsterCard monsterCard) {
		
		/*
		 * For this method, we'll first use a simple Statement to update
		 * a card's name. By doing so, we'll see that simple Statements
		 * leave us vulnerable to SQL injection.
		 * 
		 * NOTE: We're going to swap out the simple Statement for a Prepared Statement.
		 * This will prevent SQL injection by allowing to parameterize our SQL queries.
		 * When working with PreparedStatement, your SQL is precompiled, enforcing
		 * types on your parameters.
		 */
		
		Connection conn = null;
		PreparedStatement stmt = null;
		/*
		 * The question mark is used to denote a parameter.
		 */
		final String SQL = "update card set card_name = ? where card_name = 'Dark Magician'";
		
		try {
			conn = ConnectionFactory.getConnection();
			stmt = conn.prepareStatement(SQL);
			/*
			 * Before you execute a PreparedStatement, you need to fill in your parameters.
			 */
			stmt.setString(1, monsterCard.getName()); //set parameter 1
			stmt.execute();
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			ConnectionCloser.closeResource(conn);
			ConnectionCloser.closeResource(stmt);
		}

	}

	@Override
	public void delete(MonsterCard monsterCard) {
		// TODO Auto-generated method stub

	}

}
