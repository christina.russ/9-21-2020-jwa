package com.revature.service.test;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.revature.model.MonsterCard;
import com.revature.repository.MonsterCardRepositoryImpl;
import com.revature.service.MonsterCardService;

/*
 * This test suite should test our MonsterCardService and
 * ONLY our MonsterCardService.
 */
public class MonsterCardServiceTest {

	//MonsterCardService is our object under test.
	@InjectMocks //Inject your mocks into the object under test
	private static MonsterCardService monsterCardService;
	
	//This tells Mockito to mock this type for us
	@Mock
	private MonsterCardRepositoryImpl monsterCardRepository;
	
	@BeforeClass
	public static void setUp() {
		monsterCardService = new MonsterCardService();
	}
	
	@Before
	public void before() {
		/*
		 * We need to initialize our Mocks using the static init mocks methods
		 */
		MockitoAnnotations.openMocks(this);
	}
	
	/*
	 * This test is NOT a unit test. It's not a unit test because it does not test
	 * only a single piece of functionality (the smallest unit) of this project. It
	 * actually tests:
	 * 
	 * 1) The service method "findAllMonsterCards" - we meant to test this
	 * 2) It transitively tests the "findAllMonsterCards" on the repository layer as well.
	 *    This is a problem for several reasons. One: We can't isolate any potential bugs
	 *    as easily if we've called several methods. How do you know which method contains
	 *    the bug? Two: We've actually hit our database to run a test. This slows down
	 *    our test suite AND we've actually used our "production" data to run the tests.
	 *    
	 *This is where "mocking" comes in. When we mock an object using a framework designed
	 *for mocking out our dependencies, we can use "stubs". Stubs are used for state
	 *verification. In essence, you return "canned data (e.g. dummy data) when you make
	 *a method call rather than calling the actual method. This is what we want as we are
	 *ONLY concerned with testing the service layer's findAllMonsterCards method right
	 *now.
	 *
	 *Once you mock the dependency (MonsterCardRepository), this test will
	 *break.
	 */
	@Test //NOT GOOD
	public void testFindAllMonsterCards() {
		//When we call findAllMonsterCards, we know that we
		//should get a list of MonsterCards.
		
		List<MonsterCard> retrievedCards = this.monsterCardService.findAllMonsterCards();
		 
		//Modify my POM to add Jupiter to POM so that I can use non-deprecated method.
		Assert.assertEquals(retrievedCards.get(0).getName(), "Dragon Man");
		
	}
	
	/*
	 * We're going to use a mocking framework known as Mockito. Like JUnit,
	 * it is annotation-driven. 
	 * 
	 * When using Mockito, keep in mind:
	 * 
	 * 1) You cannot mock a final class as Mockito needs to extend a class
	 *    in order to mock it.
	 * 2) You cannot mock static methods.
	 */
	@Test
	public void betterTestFindAllMonsterCards() {
		
		//In this case, we are going to stub a method call, meaning that
		//we will return canned values when we call methods.
		
		Mockito.when(this.monsterCardRepository.findAllMonsterCards())
		.thenReturn(Arrays.asList(
				new MonsterCard(1, "Smitty", 12, false), 
				new MonsterCard(2, "Average Joe", 21, true)
				));
		
		List<MonsterCard> retrievedCards = this.monsterCardRepository.findAllMonsterCards();
	
		Assert.assertEquals(retrievedCards.get(0).getName(), "Smitty");
	}
	
	
}
