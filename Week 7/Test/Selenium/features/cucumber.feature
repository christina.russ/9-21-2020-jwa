#Cucumber is a BDD framework. When we use the term "BDD", we are referring to "Behavior
#Driven Development". BDD is still TDD, but it emphasizes thinking about our tests at
#high level. In other words, technical aspects of the program/application are described
#using a language that is acessible to everyone on your team (e.g. business analysts and
#other nontechnical members of your team.

#Cucumber allows us to achieve this by giving us access to a language called "Gherkin".
#Gherkin has several keywords and its own syntax which allows us to describe potential
#test cases at a high level. After doing this, Cucumber can provide developers with
#the additional utility of generating methods (but unimplemented) that they would then
#implement.

#A Cucumber Feature is a collection of scenarios and steps to complete those scenarios.
Feature: 

	#A Cucumber Scenario defines some busines logic or way in which an actor can use my
	#application. A scenario is composed of several different steps that the actor (end
	#user) may take in that scenario.
	Scenario: Logging Into SwagLabs
		#Given describes some pre-condition that must be met before an actor can perform actions.
		Given a user is on the login page of SwagLabs
		#When describes the actions that an actor takes in this situation
		When the user inputs their username
		And the user inputs their password
		#But the password is wrong
		And the user hits the enter button
		#Then describes what the expected results of the actor taking those actions should be
		Then the user ends up on the inventory page
		
	#A Scenario Outline implies that we want to feed data into our test methods. This 
	#allows us to take a data-driven approach to testing.
	#Scenario Outline: Logging Into SwagLabs using multiple sets of data
		#Given a user is on the login page of SwagLabs
		#When the user inputs <username> as the username and <password> as the password
		#And the user hits the enter button
		#Then the user ends up on the inventory page
		
		#Where do we get the data we want to feed in from?
		#Examples:
			#|username       |password    |
			#|standard_user  |secret_sauce|
			#|locked_out_user|secret_sauce|
			#|problem_user   |secret_sauce|
			
#Sometimes, many of our scenarios share pre-conditions or steps. In those
#cases, we can use the "Background" keyword to provide a shared context.

#Background: Given a user is on the home page of SwagLabs

#Scenario: 
	#When a user provides correct credentials
	#Then the user ends up on the inventory page
	
#Scenario:
	#When a user provides incorrect credentials
	#Then the user stays on the home page
					

	