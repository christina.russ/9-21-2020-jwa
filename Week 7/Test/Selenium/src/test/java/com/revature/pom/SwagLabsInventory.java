package com.revature.pom;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SwagLabsInventory {

	@FindBy(className = "btn_inventory")
	public List<WebElement> addToCartButtons;
	@FindBy(id = "shopping_cart_container")
	public WebElement cartIcon;
	
	public SwagLabsInventory(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	//We'll need a method for adding every item to our cart
	public void addAllToCart() {
		for(WebElement el : addToCartButtons) {
			el.click();
		}
	}
	
	//We'll also need a method for clicking the cart icon
	public void checkCart() {
		cartIcon.click();
	}
}
