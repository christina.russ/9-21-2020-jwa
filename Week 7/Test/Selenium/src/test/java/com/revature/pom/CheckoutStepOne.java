package com.revature.pom;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CheckoutStepOne {

	@FindBy(id = "first-name")
	private WebElement firstNameBox;
	@FindBy(id = "last-name")
	private WebElement lastNameBox;
	@FindBy(id = "postal-code")
	private WebElement zipCode;
	@FindBy(className = "cart_button")
	private WebElement continueButton;
	
	public CheckoutStepOne(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	//I need a method for entering user information into the input boxes
	public void enterInformation() {
		firstNameBox.sendKeys("Christina");
		lastNameBox.sendKeys("Russ");
		zipCode.sendKeys("60643");
	}
	
	//I need a method for pressing the "continue" button
	public void clickContinue() {
		continueButton.click();
	}
}
