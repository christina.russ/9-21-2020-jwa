package com.revature.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class SwagLabsShoppingSpree {

	private WebDriver driver;
	
	/*
	 * Recall that we downloaded our browser-specific driver and placed it
	 * in our project directory. We must tell Selenium where this driver
	 * is and provide the proper implementation of the WebDriver interface.
	 */
	
	@BeforeClass
	public void setUp() {
		System.setProperty("webdriver.chrome.driver", "drivers/chromedriver.exe");
		driver = new ChromeDriver();
		//Simulate navigating to saucedemo's home page.
		driver.get("https://www.saucedemo.com");
	}
	
	/*
	 * Our first runthrough will entail putting in our user credentials on
	 * saucedemo.com and logging into the website.
	 */
	@Test
	public void standardLogin() {
		WebElement usernameBox = driver.findElement(By.id("user-name"));
		WebElement passwordBox = driver.findElement(By.id("password"));
		WebElement loginButton = driver.findElement(By.id("login-button"));
		
		//Now that I've located all of the elements I want to interact with, I can
		//perform actions on them.
		usernameBox.sendKeys("standard_user");
		passwordBox.sendKeys("secret_sauce");
		loginButton.click();
		
		//Check that I'm on the correct web page after entering my credentials.
		//Will update with something reasonable as saucedemo uses the same title
		//for every single page.
		
	}
	
	//Quit the driver after you're done running that test!
	@AfterClass
	public void tearDown() {
		driver.quit();
	}
}
