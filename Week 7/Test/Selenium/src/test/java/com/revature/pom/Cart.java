package com.revature.pom;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Cart {

	@FindBy(linkText = "CHECKOUT")
	private WebElement checkoutButton;
	
	public Cart(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	//We'll need a method to click the checkout button
	public void checkout() {
		checkoutButton.click();
	}
}
