package com.revature.pom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/*
 * This will serve as a page object model (POM). The POM is a design pattern which
 * allows us to create repositories of web elements ahead of time so that we don't
 * have to continue to manually grab web elements with our WebDriver.
 * 
 * Typically, you have a POM for each web page you wish to interact with.
 * 
 * A POM contains a list of the web elements you'll need to interact with AND some
 * useful methods for interacting with said web elements.
 */
public class SwagLabsHome {

	public WebDriver driver;
	
	//List of our web elements
	@FindBy(id = "user-name")
	public WebElement usernameBox;
	@FindBy(id = "password")
	public WebElement passwordBox;
	@FindBy(id = "login-button")
	public WebElement loginButton;
	
	//This constructor takes a driver from elsewhere.
	public SwagLabsHome(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	
	public void login() {
		/*
		 * I have chosen to manually grab the WebElements myself using the
		 * driver. That said, you don't have to grab WebElements like this
		 * because Selenium allows you to eagerly grab WebElements using the
		 * PageFactory. Using the PageFactory, you need only provide the
		 * Selenium locator that should be used to find WebElements using
		 * the @FindBy annotation over the WebElement.
		 * 
		 * Note: I've commented out the following lines of code because they
		 * are no longer needed when using PageFactory.
		 */
//		usernameBox = driver.findElement(By.id("user-name"));
//		passwordBox = driver.findElement(By.id("password"));
//		loginButton = driver.findElement(By.id("login-button"));
		
		usernameBox.sendKeys("standard_user");
		passwordBox.sendKeys("secret_sauce");
		loginButton.click();
	}
}
