package com.revature.selenium;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.revature.pom.Cart;
import com.revature.pom.CheckoutStepOne;
import com.revature.pom.SwagLabsHome;
import com.revature.pom.SwagLabsInventory;

/*
 * We're going to refactor our previous ShoppingSpree test class in order to
 * take advantage of our page object model(s).
 */
public class SwagLabsShoppingSpreeUpgrade {

	private WebDriver driver;
	private SwagLabsHome swagLabsHome;
	private SwagLabsInventory swagLabsInventory;
	private Cart cart;
	private CheckoutStepOne checkoutStepOne;
	
	@BeforeClass
	public void setUp() {
		System.setProperty("webdriver.chrome.driver", "drivers/chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("https://www.saucedemo.com");
		
		/*
		 * As you have probably noticed, Selenium scripts tend to move quite
		 * quickly. This is good as a replacement for manually testing your
		 * UI, but there is a downside to this: Sometimes Selenium attempts
		 * to interact with an element before it even exists on the web page.
		 * When this happens, you will get NoSuchElementException.
		 * 
		 * In order to combat this, we use what we call "Selenium Waits".
		 * Selenium Waits tell our driver to wait for a specified amount of
		 * time before it attempts to interact with a WebElement.
		 * 
		 * Waits can be useful, but keep in mind that using Waits slows down
		 * your test suites.
		 * 
		 * There are 3 types of waits:
		 * 
		 * 1) Implicit Waits
		 * 2) Explicit Waits
		 * 3) Fluent Waits
		 * 
		 * Let's start with our Implicit wait. Implicit waits allow us to wait
		 * for a specified amount of time before a NoSuchElementException is
		 * thrown.
		 * 
		 * NOTE: The website that we're using hasn't given us issues with
		 * rendering times, so using waits here isn't practical.
		 * 
		 * I do not prefer implicit waits over explicit waits because they
		 * are generic in how they wait for web elements to appear.
		 */
		
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.MILLISECONDS);
	}
	
	@Test
	public void login() {
		//Let's use our page object model instead.
		swagLabsHome = new SwagLabsHome(driver);
		swagLabsHome.login();
	}
	
	/*
	 * By default, tests in TestNG run in alphabetical order. That said, sometimes
	 * we need our tests to run in a specific order. If they don't, they might fail.
	 * That said, we can use the "dependsOnMethods" attribute to specify that a 
	 * specify that a certain test should NOT be run before the methods it depends on.
	 */
	@Test(dependsOnMethods = {"login"})
	public void shop() {
		swagLabsInventory = new SwagLabsInventory(driver);
		/*
		 * Let's now use an Explicit wait. Explicit waits are different from
		 * Implicit waits in that they allow us to specify conditions that
		 * must be met before elements are interacted with. For instance,
		 * we can wait for an element be visible before we interact with it.
		 * 
		 * Explicit waits work on a per element basis. This means that your
		 * wait is applied to specific elements.
		 */
		
		/*
		 * This explicit wait will wait up to 5 seconds for the visibility of all
		 * of the addToCart buttons.
		 */
//		WebDriverWait wait = new WebDriverWait(driver, 5);
//		wait.until(ExpectedConditions.visibilityOfAllElements(swagLabsInventory.addToCartButtons));
		
		/*
		 * We can optionally choose to use a FluentWait, which like an Explicit Wait,
		 * allows us to use ExpectedConditions. We also, however, get to modify or
		 * customize our polling time for our wait. While the Explicit Wait has a default
		 * polling time of 500ms, the Fluent wait's polling time can be modified.
		 */
		
		FluentWait<WebDriver> fwait = new FluentWait<>(driver).withTimeout(Duration.ofMillis(5000))
				.pollingEvery(Duration.ofMillis(250))
				.ignoring(NoSuchElementException.class);
		
		fwait.until(ExpectedConditions.visibilityOfAllElements(swagLabsInventory.addToCartButtons));
		
		swagLabsInventory.addAllToCart();
		swagLabsInventory.checkCart();
	}
	
	@Test(dependsOnMethods = {"shop"})
	public void checkout() {
		cart = new Cart(driver);
		cart.checkout();
	}
	
	@Test(dependsOnMethods = {"checkout"})
	public void checkoutStepOne() {
		checkoutStepOne = new CheckoutStepOne(driver);
		checkoutStepOne.enterInformation();
		checkoutStepOne.clickContinue();
	}
	
	@Test(dependsOnMethods = {"checkoutStepOne"})
	public void finishShopping() {
		driver.findElement(By.linkText("FINISH")).click();
	}
	
	@AfterClass
	public void tearDown() {
		/*
		 * I've made the thread sleep for 5 seconds because the browser closes
		 * way too quickly otherwise and we don't get to see whether or not
		 * we're on the right page.
		 */
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		driver.quit();
	}
}
