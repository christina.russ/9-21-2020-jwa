package com.revature.cukes;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

/*
 * I'll use this file to set up a special test runner for my Cucumber tests.
 * Please note that this test runner is provided by JUnit.
 * 
 * Note that we run this file (not the step definition file) to run the
 * Cucumber tests.
 */

@CucumberOptions(features = "features/cucumber.feature",
glue = "com.revature.cukes")
@RunWith(Cucumber.class)
public class CucumberTestRunner {

}
