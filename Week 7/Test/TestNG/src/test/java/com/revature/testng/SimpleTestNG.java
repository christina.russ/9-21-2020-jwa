package com.revature.testng;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

/*
 * Like JUnit, TestNG is a testing framework for Java. It is annotation-based,
 * but there can be some XML configuration if you want to use TestNG groups
 * and suites. 
 * 
 * The annotations available to you in TestNG are considered a superset of the
 * annotations available to you in JUnit. That is to say, you have access to
 * many of the standard JUnit annotations plus additional annotations. TestNG
 * also gives you the ability to easily control the order in which your texts
 * are executed and perform data-driven tests.
 * 
 * P.S. TestNG stands for Test Next Generation.
 */
public class SimpleTestNG {

	//Let's take a look at our TestNG annotations.
	
	/*
	 * TestNG allows you to define custom test suites which include certain
	 * test classes and packages. BeforeSuite runs before the entire suite of tests.
	 */
	@BeforeSuite
	public void beforeSuite() {
		System.out.println("Before Suite");
	}
	
	/*
	 * TestNG allows you to create custom groups for your tests. For instnace,
	 * you can place in a test in a group called "smoke". This means that this
	 * method would run before tests in said group (and other groups).
	 */
	@BeforeGroups
	public void beforeGroups() {
		System.out.println("Before Groups");
	}
	
	@BeforeClass
	public void beforeClass() {
		System.out.println("Before Class");
	}
	
	@BeforeMethod
	public void beforeMethod() {
		System.out.println("Before Method");
	}
	
	/*
	 * If you want to ignore a test in TestNG, you use the enabled attribute.
	 */
	@Test(enabled = false)
	public void test() {
		
	}
	
	/*
	 * You can add a test to a group by using the "groups" attribute. Note that
	 * you are simultaneously creating the group by doing this.
	 */
	@Test(groups = {"smoke", "regression"})
	public void test2() {
		System.out.println("Test 2");
	}
	
	@Test(groups = {"smoke"})
	public void test3() {
		System.out.println("Test 3");
	}
	
	@Test(groups = {"regression"})
	public void test4() {
		System.out.println("Test 4");
	}
	
	@AfterMethod
	public void afterMethod() {
		System.out.println("After Method");

	}
	
	@AfterClass
	public void afterClass() {
		System.out.println("After Class");
	}
	
	//Executed after TestNG groups have finished running.
	@AfterGroups
	public void afterGroups() {
		System.out.println("After Groups");
	}
	
	//Executed after a TestNG suite is finished running.
	@AfterSuite
	public void afterSuite() {
		System.out.println("After Suite");
	}
}
