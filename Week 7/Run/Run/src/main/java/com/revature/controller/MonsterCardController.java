package com.revature.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.revature.model.MonsterCard;
import com.revature.service.MonsterCardService;

@RestController("monsterCardController")
@RequestMapping(path = "/monstercard")
@CrossOrigin(origins = {"http://localhost:4200"})
public class MonsterCardController {

	@Autowired
	private MonsterCardService monsterCardService;
	
	@GetMapping(path = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<MonsterCard> findAll(){
		return this.monsterCardService.findAll();
	}
}
