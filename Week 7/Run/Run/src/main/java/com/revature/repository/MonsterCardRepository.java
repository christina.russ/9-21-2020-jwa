package com.revature.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.revature.model.MonsterCard;

public interface MonsterCardRepository extends JpaRepository<MonsterCard, Integer>{

	List<MonsterCard> findAll();
}
