package com.revature.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.revature.model.MonsterCard;
import com.revature.repository.MonsterCardRepository;

@Service("monsterCardService")
public class MonsterCardService {

	@Autowired
	private MonsterCardRepository monsterCardRepository;
	
	public List<MonsterCard> findAll(){
		return this.monsterCardRepository.findAll();
	}
}
