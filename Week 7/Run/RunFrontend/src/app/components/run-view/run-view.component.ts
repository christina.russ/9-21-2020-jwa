import { Component, OnInit } from '@angular/core';
import { RunService } from 'src/app/services/run.service';

@Component({
  selector: 'app-run-view',
  templateUrl: './run-view.component.html',
  styleUrls: ['./run-view.component.css']
})
export class RunViewComponent implements OnInit {

  constructor(private runService:RunService) { }

  ngOnInit(): void {
    this.findAllMonsters()
  }

  cards:Object[] = []

  findAllMonsters(){
    this.runService.findAllMonsterCards().subscribe(
      (data) => {
        console.log(data)
        this.cards = data;
      },
      () => {
        console.log("sorry something went wrong")
      }
    )
  }

}
