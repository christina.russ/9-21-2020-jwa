import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RunViewComponent } from './components/run-view/run-view.component';

const routes: Routes = [
  {
    path: "run",
    component: RunViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
