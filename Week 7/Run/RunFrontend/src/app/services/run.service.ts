import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class RunService {

  constructor(private httpClient:HttpClient) { }

  findAllMonsterCards(){
    return this.httpClient.get('http://localhost:8087/monstercard/all') as Observable<Object[]>
  }
}
