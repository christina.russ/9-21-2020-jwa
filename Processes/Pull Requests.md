# Making A Pull Request

> Throughout training, you will often be tasked with completing a part of an application that you and your coworkers have been building. When working on an application with your peers, you'll need to first work on the changes locally and then ask your scrum master for permission to integrate those changes into the main branch.

## Steps To Making a Pull Request

A pull request allows you to request that changes that you have made on one branch be merged into another branch. In order to make a pull request on GitLab, you should the [instructions provided by GitLab](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html).

Before making your pull request, make sure that you:

1. Have created a feature-specific branch to complete your work on.
2. Have performed any other merges necessary. For instance, maybe you and your peers have decided that you will all work on 3 different branches and then merge all of your changes on one branch. Once you've merged into that branch, you plan to make a merge request for that branch.
3. Have sorted all of your merge conflicts. I will NOT fix your merge conflicts for you. It's good practice for you to fix your own merge conflicts so that you learn how to deal with them when they occur.
4. Are up-to-date with the branch you want to merge your changes into. 
